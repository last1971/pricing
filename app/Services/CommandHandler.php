<?php

namespace App\Services;

use App\Services\ExceptionHandlers\MainExceptionHandler;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Collection;

class CommandHandler
{
    /**
     * @return void
     * @throws BindingResolutionException
     */
    public function handle(): void
    {
        /** @var Collection $commandQueue */
        $commandQueue = app()->make('CommandQueue');
        /** @var MainExceptionHandler $exceptionHandler */
        $exceptionHandler = app()->make(MainExceptionHandler::class);
        if ($commandQueue->isNotEmpty()) {
            $command = $commandQueue->shift();
            try {
                $command->execute();
            }  catch (Exception $e) {
                $exceptionHandler->obtain($command, $e);
            }
        }
    }
}
