<?php

namespace App\Services;

use App\Interfaces\ICanable;
use App\Interfaces\ICommand;
use App\Interfaces\IUObject;

class QuerySearchFabric
{
    /**
     * @param IUObject $object
     * @return ICommand
     */
    public static function create(IUObject $object): ICommand
    {
        return app('PossibleSearchStrategies')[$object->get('searchStrategy')]::create($object);
    }
}
