<?php

namespace App\Services\ExceptionHandlers;

use App\Interfaces\ICommand;
use App\Interfaces\IExceptionHandler;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Collection;
use Log;

class MainExceptionHandler implements IExceptionHandler
{

    /**
     * @param string $command
     * @param string $exception
     * @param IExceptionHandler $exceptionHandler
     * @return void
     * @throws BindingResolutionException
     */
    public static function addExceptionHandler(
        string $command, string $exception, IExceptionHandler $exceptionHandler
    ): void
    {
        $exceptionHandlers = app()->make('ExceptionHandlers');
        $exceptionHandlers->put(md5($command . $exception), $exceptionHandler);
    }

    /**
     * @param ICommand $command
     * @param Exception $exception
     * @return void
     * @throws BindingResolutionException
     */
    public function obtain(ICommand $command, Exception $exception): void
    {
        $hash = md5(get_class($command) . get_class($exception));

        /** @var Collection $exceptionHandlers */
        $exceptionHandlers = app()->make('ExceptionHandlers');

        /** @var IExceptionHandler $exceptionHandler */
        $exceptionHandler = $exceptionHandlers->get($hash, $this);

        app('LastException', [$exception]);

        $exceptionHandler->handle($command, $exception);
    }

    /**
     * @param ICommand $command
     * @param Exception $exception
     * @return void
     */
    public function handle(ICommand $command, Exception $exception): void
    {
        Log::error($exception->getMessage(), [
            'Command:' => $command,
            'Exception:' => $exception,
        ]);
    }
}
