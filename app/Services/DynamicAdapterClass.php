<?php

namespace App\Services;

use App\Interfaces\IClassable;
use App\Interfaces\IUObject;
use Illuminate\Support\Collection;
use App\Services\Methods\MethodGet;
use App\Services\Methods\MethodSet;
use App\Services\Methods\MethodVoid;
use ReflectionClass;
use ReflectionMethod;
use ReflectionNamedType;

class DynamicAdapterClass implements IClassable
{
    /** @var Collection  */
    private Collection $methods;

    public function __construct()
    {
        $this->methods = collect([
            'void0' => new MethodVoid(),
            'void1' => new MethodSet(),
        ]);
    }

    /**
     * @param ReflectionClass $class
     * @return string
     */
    public function getClassName(ReflectionClass $class): string
    {
        return $class->getShortName() . 'Adapter';
    }

    /**
     * @return string
     */
    public function createConstructor(): string
    {
        $iUObject = IUObject::class;
        $response = "private $iUObject \$uObject; ";
        $response .= "public function __construct($iUObject \$uObject) ";
        $response .= "{ \$this->uObject = \$uObject; }";
        return $response;
    }

    /**
     * @param ReflectionMethod $method
     * @return string
     */
    /** @suppress PhanUndeclaredInvokeInCallable */
    public function createMethod(ReflectionMethod $method): string
    {
        $className = $this->getClassName($method->getDeclaringClass());
        $parameters = $method->getParameters();
        $returnType = $method->getReturnType();
        $returnTypeName = $returnType instanceof ReflectionNamedType ? $returnType->getName() : '';
        $methodable = $this
            ->methods
            ->get($returnTypeName . count($parameters), new MethodGet());
        return $methodable->createMethod($method, $className);
    }
}
