<?php

namespace App\Services;

use App\Interfaces\IUObject;
use Illuminate\Support\Collection;

/** @template T */
class UObject implements IUObject
{

    /**
     * @var Collection
     */
    private Collection $properties;

    /**
     * @param array $init
     */
    public function __construct(array $init = [])
    {
        $this->properties = collect($init);
    }

    /**
     * @param string $key
     * @return T
     */
    public function get(string $key): mixed
    {
        return $this->properties->get($key);
    }

    /**
     * @param string $key
     * @param T $value
     * @return void
     */
    public function set(string $key, mixed $value): void
    {
        $this->properties->put($key, $value);
    }

    /**
     * @return string
     */
    public function hash(): string
    {
        return md5($this->properties
            ->reduce(
                fn($carry, $value, $key) => $carry . (is_string($value) ? $key . ':' . $value . ';' : ''),
                '',
            )
        );
    }
}
