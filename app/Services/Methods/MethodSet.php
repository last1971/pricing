<?php

namespace App\Services\Methods;

use App\Interfaces\IMethodable;
use Illuminate\Support\Str;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionParameter;

class MethodSet implements IMethodable
{

    /**
     * @param ReflectionMethod $method
     * @param array $args
     * @return string
     */
    public function createMethod(ReflectionMethod $method, ...$args): string
    {
        $methodName = $method->getName();
        $name = Str::camel(Str::substr($methodName, 3));
        $parameter = $method->getParameters()[0];
        $response = "public function $methodName(";
        $response .= $this->createParameter($parameter);
        $response .= "): void { ";
        $parameterName = $parameter->getName();
        $response .= "\$this->uObject->set('$name', \$$parameterName); }";
        return $response;
    }

    /**
     * @param ReflectionParameter $parameter
     * @return string
     */
    private function createParameter(ReflectionParameter $parameter): string
    {
        $type = $parameter->getType();
        $returnTypeName = $type instanceof ReflectionNamedType ? $type->getName() : '';
        $name = $parameter->getName();
        $response = "$returnTypeName $$name";
        if ($parameter->isDefaultValueAvailable()) {
            $defaultValue = $parameter->getDefaultValue();
            $response .= "=$defaultValue";
        }
        return $response;
    }
}
