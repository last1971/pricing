<?php

namespace App\Services\Methods;

use App\Interfaces\IMethodable;
use App\Interfaces\IUObject;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use ReflectionMethod;
use ReflectionNamedType;

class MethodGet implements IMethodable
{
    /**
     * @param ReflectionMethod $method
     * @param array $args
     * @return string
     */
    public function createMethod(ReflectionMethod $method, ...$args): string
    {
        $methodName = $method->getName();
        $name = Str::camel(Str::substr($methodName, 3));
        $returnType = $method->getReturnType();
        $returnTypeName = $returnType instanceof ReflectionNamedType ? $returnType->getName() : '';
        $response = "public function $methodName(): $returnTypeName";
        $response .= $name === 'this' && $returnTypeName === IUObject::class
            ? "{ return \$this->uObject; }"
            : "{ return \$this->uObject->get('$name'); }";
        return $response;
    }
}
