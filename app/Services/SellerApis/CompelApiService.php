<?php

namespace App\Services\SellerApis;

use App\Exceptions\CompelException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class CompelApiService
{
    public function __construct(private Client $client)
    {
    }

    /**
     * @param string $method
     * @param array $params
     * @return array
     * @throws CompelException
     * @throws GuzzleException
     */
    private function method(string $method, array  $params = []): array
    {
        $queryParams = array_merge($params, ['user_hash' => env('COMPEL_API_HASH')]);

        $request = [
            'id' => 1,
            'method' => $method,
            'params' => $queryParams
        ];

        $res = $this->client->request('POST', env('COMPEL_API_URL'), ['json'   => $request]);

        $response = json_decode($res->getBody()->getContents(), true);

        if ($response['error'] !== null) {
            throw new CompelException($request, $response, $response['error']['message']);
        }

        return $response;
    }

    /**
     * @param string $name
     * @return array
     * @throws CompelException
     * @throws GuzzleException
     */
    public function searchByName(string $name): array
    {
        $params = ['query_string'  => $name . '*'];
        return $this->method('search_item_ext', $params);
    }

    /**
     * @param array $params
     * @return array
     * @throws CompelException
     * @throws GuzzleException
     */
    public function orders(array $params = []): array
    {
        $queryParams = array_merge(['order_sales_id' => 'desc'], $params);
        return $this->method('sales_info', $queryParams);
    }

    /**
     * @param string $name
     * @return array
     * @throws CompelException
     * @throws GuzzleException
     */
    public function searchInCenter(string $name): array
    {
        $params = [
            'query_string'  => $name . '*',
            'calc_price' => true,
            'calc_qty' => true,
        ];
        return $this->method('search_item_name_h', $params);
    }

}
