<?php

namespace App\Services\SellerApis;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class PromelecApiService
{
    /**
     * @param string $method
     * @param array|null $params
     * @return array
     * @throws GuzzleException
     * @throws Exception
     */
    public function method(string $method, array  $params = null): array
    {
        $client = new Client;

        $request = [
            'login'         => env('PROM_LOGIN'),
            'password'      => md5(env('PROM_PASS')),
            'customer_id'   => env('PROM_ID'),
            'method'        => $method,
        ];
        $request = $params ? array_merge($params, $request) : $request;
        $res = $client->request(
            'POST',
            env('PROM_API_URL'),
            [
                'json'   => $request
            ]
        );

        $response = json_decode($res->getBody()->getContents(), true);

        if (isset($response['error'])) {
            throw new Exception($response['error']);
        }

        return $response;
    }

    /**
     * @param string $name
     * @return array
     * @throws GuzzleException
     */
    public function searchByName(string $name): array
    {
        $params = [
            'name'      => $name,
            'extended'  => 1
        ];
        return $this->method('items_data_find', $params);
    }
}
