<?php

namespace App\Services;

use App\Interfaces\ISearchStrategyable;
use App\Interfaces\IUObject;

class ObjectSearchStrategy implements ISearchStrategyable
{
    /**
     * @param IUObject $object
     */
    public function __construct(private readonly IUObject $object)
    {
    }

    /**
     * @return string|null
     */
    public function createSearchStrategy(): string|null
    {
        $strategies = array_keys(app('PossibleSearchStrategies'));
        foreach ($strategies as $strategy) {
            if ($this->object->get($strategy)) {
                return $strategy;
            }
        }
        return null;
    }

    /**
     * @param string $strategy
     * @return void
     */
    public function setSearchStrategy(string $strategy): void
    {
        $this->object->set('searchStrategy', $strategy);
    }
}
