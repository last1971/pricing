<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Throwable;

class ApiException extends Exception
{
    /**
     * @var array
     */
    protected array $errors;

    public function __construct($errors = null, string $message = null, int $code = null, ?Throwable $previous = null)
    {
        $this->errors = is_array($errors) ? $errors : $this->getDefaultErrors($errors);
        parent::__construct(
            $message ?? $this->getDefaultMessage(),
            $code ?? $this->getDefaultCode(),
            $previous
        );
    }

    /**
     * @return Response|Application|ResponseFactory
     */
    public function render(): Response|Application|ResponseFactory
    {
        return response([
            'message' => $this->getMessage(),
            'errors' => $this->errors,
        ], $this->getCode());
    }

    /**
     * @param $errors
     * @return string[]
     */
    public function getDefaultErrors($errors): array
    {
        return $errors ?? [$this->getDefaultMessage()];
    }

    /**
     * @return int
     */
    public function getDefaultCode()
    {
        return 400;
    }

    /**
     * @return string
     */
    public function getDefaultMessage()
    {
        return 'Invalid request.';
    }

}
