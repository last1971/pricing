<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Throwable;

class CompelException extends Exception
{
    /**
     * @param array $request
     * @param array $response
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        private array $request,
        private array $response,
        string $message = "",
        int $code = 0,
        Throwable $previous = null,
    )
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return void
     */
    public function report()
    {
        Log::error($this->getMessage(), [
            'Request:' => $this->request,
            'Response:' => $this->response,
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()->json([
            'message' => $this->getMessage(),
            'exception' => 'CompelApiException',
            'errors' => [],
        ], 500);
    }
}
