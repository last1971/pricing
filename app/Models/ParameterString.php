<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\ParameterString
 *
 * @property int $id
 * @property string $value
 * @property int $parameter_name_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ParameterName|null $parameterName
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ParameterValue[] $parameterValues
 * @property-read int|null $parameter_values_count
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterString newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterString newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterString query()
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterString whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterString whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterString whereParameterNameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterString whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterString whereValue($value)
 * @mixin \Eloquent
 */
class ParameterString extends BaseModel
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = ['value', 'parameter_name_id'];

    /**
     * @return BelongsTo
     */
    public function parameterName(): BelongsTo
    {
        return $this->belongsTo(ParameterName::class);
    }

    /**
     * @return HasMany
     */
    public function parameterValues(): HasMany
    {
        return $this->hasMany(ParameterValue::class);
    }
}
