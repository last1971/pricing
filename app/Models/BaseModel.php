<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BaseModel
 *
 * @template T
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModel query()
 * @mixin \Eloquent
 */
class BaseModel extends Model
{
    /**
     * @param string $key
     * @return T
     */
    public function get(string $key): mixed
    {
        return $this->getAttribute($key);
    }

    /**
     * @param string $key
     * @param T $value
     * @return void
     */
    public function set(string $key, mixed $value): void
    {
        $this->setAttribute($key, $value);
    }
}
