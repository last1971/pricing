<?php

namespace App\Models;

use App\Traits\UuidPrimaryKeyTrait;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Price
 *
 * @mixin Eloquent
 * @mixin Builder
 * @property-read \App\Models\Currency|null $currency
 * @property-read \App\Models\Good|null $good
 * @method static Builder|Price newModelQuery()
 * @method static Builder|Price newQuery()
 * @method static Builder|Price query()
 * @property-read \App\Models\Warehouse|null $warehouse
 * @property string $id
 * @property int $value
 * @property int $min_quantity
 * @property int $max_quantity
 * @property int $currency_id
 * @property int $warehouse_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Price whereCreatedAt($value)
 * @method static Builder|Price whereCurrencyId($value)
 * @method static Builder|Price whereId($value)
 * @method static Builder|Price whereMaxQuantity($value)
 * @method static Builder|Price whereMinQuantity($value)
 * @method static Builder|Price whereUpdatedAt($value)
 * @method static Builder|Price whereValue($value)
 * @method static Builder|Price whereWarehouseId($value)
 */
class Price extends Model
{
    use HasFactory, UuidPrimaryKeyTrait;

    /**
     * @var string[]
     */
    protected $fillable = ['value', 'min_quantity', 'max_quantity', 'currency_id', 'warehouse_id'];

    /**
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @return BelongsTo
     */
    public function warehouse(): BelongsTo
    {
        return $this->belongsTo(Warehouse::class);
    }
}
