<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\ParameterValue
 *
 * @property int $id
 * @property int|null $parameter_string_id
 * @property int|null $numeric_value
 * @property int|null $fraction
 * @property int $parameter_name_id
 * @property int|null $parameter_unit_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ParameterName|null $parameterName
 * @property-read \App\Models\ParameterString|null $parameterString
 * @property-read \App\Models\ParameterUnit|null $parameterUnit
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterValue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterValue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterValue query()
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterValue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterValue whereFraction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterValue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterValue whereNumericValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterValue whereParameterNameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterValue whereParameterStringId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterValue whereParameterUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterValue whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Good|null $good
 * @property int $good_id
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterValue whereGoodId($value)
 */
class ParameterValue extends BaseModel
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'parameter_string_id', 'numeric_value', 'fraction', 'parameter_name_id', 'parameter_unit_id', 'product_id'
    ];

    /**
     * @return BelongsTo
     */
    public function parameterString(): BelongsTo
    {
        return $this->belongsTo(ParameterString::class);
    }

    /**
     * @return BelongsTo
     */
    public function parameterName(): BelongsTo
    {
        return $this->belongsTo(ParameterName::class);
    }

    /**
     * @return BelongsTo
     */
    public function parameterUnit(): BelongsTo
    {
        return $this->belongsTo(ParameterUnit::class);
    }

    /**
     * @return BelongsTo
     */
    public function good(): BelongsTo
    {
        return $this->belongsTo(Good::class);
    }

    public function parameterValue(): Attribute
    {
        return Attribute::make(
            get: function ($value, $attributes) {
                switch ($this->parameterName->type) {
                    case 'numeric':
                        return $attributes['numeric_value'] . ' ' . $this->parameterUnit->name;
                    case 'string':
                        return $this->parameterString->value;
                    case 'complex':
                        return $attributes['numeric_value'] .  ' ' . $this->parameterUnit->name . ' '
                            . $this->parameterString->value;
                    case 'bool':
                        return $attributes['numeric_value'] === 1;
                    default:
                        return '';
                }
            }
        );
    }
}
