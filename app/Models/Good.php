<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Good
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Good newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Good newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Good query()
 * @method static \Illuminate\Database\Eloquent\Builder|Good whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Good whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Good whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ParameterValue[] $parameterValues
 * @property-read int|null $parameter_values_count
 * @property-read \App\Models\Seller|null $seller
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Warehouse[] $warehouses
 * @property-read int|null $warehouses_count
 * @property int $seller_id
 * @property string $alias
 * @property string $code
 * @method static \Database\Factories\GoodFactory factory(...$parameters)
 * @method static Builder|Good whereAlias($value)
 * @method static Builder|Good whereCode($value)
 * @method static Builder|Good whereSellerId($value)
 */
class Good extends BaseModel
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = ['seller_id', 'code', 'alias'];

    /**
     * @return BelongsTo
     */
    public function seller(): BelongsTo
    {
        return $this->belongsTo(Seller::class);
    }

    /**
     * @return HasMany
     */
    public function warehouses(): HasMany
    {
        return $this->hasMany(Warehouse::class);
    }

    /**
     * @return HasMany
     */
    public function parameterValues(): HasMany
    {
        return $this->hasMany(ParameterValue::class);
    }

    /**
     * @param array $parameters
     * @return Good|null
     */
    public static function firstByStringParameters(array $parameters): Good|null
    {
        return self::whereHas('parameterValues', function (Builder $query) use ($parameters) {
            foreach (array_keys($parameters) as $parameterName) {
                $parameterName = ParameterName::firstByNameAndType($parameterName, ParameterName::String);
                $query
                    ->where('parameter_name_id', $parameterName?->id)
                    ->whereHas(
                        'parameterString',
                        function (Builder $insideQuery) use ($parameters, $parameterName) {
                            $insideQuery->where('value', $parameters[$parameterName]);
                        });

            }
        })->first();
    }

    public function alias(): Attribute
    {
        return Attribute::make(
            get: fn($value) => $value,
            set: fn($value) => mb_ereg_replace(config('app.search_replace'), '', $value),
        );
    }
}
