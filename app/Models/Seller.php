<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

/**
 * App\Models\Seller
 *
 * @property int $id
 * @property string $name
 * @property string|null $inn
 * @property string $alias
 * @property int $delivery_time
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Warehouse[] $warehouses
 * @property-read int|null $warehouses_count
 * @method static \Database\Factories\SellerFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Seller newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Seller newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Seller query()
 * @method static \Illuminate\Database\Eloquent\Builder|Seller whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seller whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seller whereDeliveryTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seller whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seller whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seller whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seller whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Good[] $goods
 * @property-read int|null $goods_count
 * @property string|null $api_class
 * @method static \Illuminate\Database\Eloquent\Builder|Seller whereApiClass($value)
 */
class Seller extends BaseModel
{
    use HasFactory;

    const Compel = 'compel';
    const Dan = 'dan';
    const Mars = 'mars';
    const Promelec = 'promelec';

    /**
     * @var string[]
     */
    protected $fillable = ['name', 'inn', 'alias', 'delivery_time', 'api_class'];

    /**
     * @return HasMany
     */
    public function goods(): HasMany
    {
        return $this->hasMany(Good::class);
    }

    /**
     * @param string $alias
     * @return Seller|null
     */
    public static function firstByAlias(string $alias): Seller|null
    {
        return Cache::remember('Seller:' .  $alias, config('timing.secondsInDay'), function () use ($alias) {
            return self::whereAlias($alias)->first();
        });
    }

    /**
     * @return Collection
     */
    public static function apiClasses(): Collection
    {
        return self::whereNotNull('api_class')->get()->mapWithKeys(function ($item, $key) {
            return [$item->alias => $item->api_class];
        });
    }
}
