<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Warehouse
 *
 * @property int $id
 * @property string $name
 * @property int $seller_id
 * @property int $delivery_time
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Good[] $goods
 * @property-read int|null $goods_count
 * @property-read \App\Models\Seller|null $seller
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse query()
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereDeliveryTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereSellerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereUpdatedAt($value)
 * @property-read \App\Models\Good|null $good
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Price[] $prices
 * @property-read int|null $prices_count
 * @property string|null $code
 * @property int $good_id
 * @property int $quantity
 * @property int $multiplicity
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereGoodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereMultiplicity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereQuantity($value)
 * @mixin \Eloquent
 */
class Warehouse extends BaseModel
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = ['code', 'good_id', 'quantity', 'multiplicity', 'delivery_time'];

    /**
     * @return BelongsTo
     */
    public function good(): BelongsTo
    {
        return $this->belongsTo(Good::class);
    }

    /**
     * @return HasMany
     */
    public function prices(): HasMany
    {
        return $this->hasMany(Price::class)->orderBy('min_quantity');
    }
}
