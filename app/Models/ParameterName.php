<?php

namespace App\Models;

use Cache;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\ParameterName
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property int|null $parameter_unit_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ParameterString[] $parameterStrings
 * @property-read int|null $parameter_strings_count
 * @property-read \App\Models\ParameterUnit|null $parameterUnit
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ParameterValue[] $parameterValues
 * @property-read int|null $parameter_values_count
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterName newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterName newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterName query()
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterName whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterName whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterName whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterName whereParameterUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterName whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ParameterName whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ParameterName extends BaseModel
{
    public const Numeric = 'numeric';
    public const String = 'string';
    public const Complex = 'complex';
    public const Bool = 'bool';

    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = ['name', 'type', 'parameter_unit_id'];

    /**
     * @return BelongsTo
     */
    public function parameterUnit(): BelongsTo
    {
        return $this->belongsTo(ParameterUnit::class);
    }

    /**
     * @return HasMany
     */
    public function parameterStrings(): HasMany
    {
        return $this->hasMany(ParameterString::class);
    }

    /**
     * @return HasMany
     */
    public function parameterValues(): HasMany
    {
        return $this->hasMany(ParameterValue::class);
    }

    /**
     * @param string $name
     * @param string $type
     * @return static|null
     */
    public static function firstByNameAndType(string $name, string $type = self::String): ?self
    {
        return Cache::remember(
            'getParameterNameByNameAndType' . $name . $type,
            config('timing.secondsInHour'),
            fn() => self::where('name', $name)->where('type', $type)->first(),
        );
    }
}
