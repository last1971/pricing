<?php

namespace App\Traits;

use App\ICommands\ApiParser;
use App\ICommands\ApiRequest;
use App\ICommands\MakeGoods;
use App\ICommands\MakeResult;

trait SearchInApiTrait
{
    public function execute(): void
    {
        ApiRequest::create($this->object)->execute();
        ApiParser::create($this->object)->execute();
        MakeGoods::create($this->object)->execute();
        MakeResult::create($this->object)->execute();
    }
}
