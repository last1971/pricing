<?php

namespace App\Traits;

use App\Interfaces\ICommand;
use App\Interfaces\IUObject;
use App\Services\CommandAdapterFabric;
use ReflectionClass;
use ReflectionNamedType;

trait CreateCommandTrait
{
    /**
     * @param IUObject $object
     * @return ICommand
     */
    public static function create(IUObject $object): ICommand
    {
        $reflect = new ReflectionClass(self::class);
        $params = $reflect->getProperties(\ReflectionProperty::IS_PRIVATE);
        $i = 0;
        do {
            /** @var ReflectionNamedType? $param */
            $param = $params[$i++]->getType();
            $interface = $param->getName();
        } while (!interface_exists($interface));
        return new self($interface === IUObject::class ? $object : CommandAdapterFabric::create($interface, $object));
    }
}
