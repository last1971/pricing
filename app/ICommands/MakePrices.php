<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IPricesable;
use App\Interfaces\IUObject;
use App\Models\Price;
use App\Traits\CreateCommandTrait;

class MakePrices implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private IPricesable $pricesable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $warehouse = $this->pricesable->getWarehouse();
        $prices = $warehouse->prices;
        $newPrices = $this->pricesable->getPrices();
        usort($newPrices, fn(IUObject $a, IUObject $b) => $a->get('min_quantity') <=> $b->get('min_quantity'));
        foreach ($newPrices as $index => $newPrice) {
            $price = [
                'warehouse_id' => $warehouse->id,
                'currency_id' => $newPrice->get('currency')->id,
                'min_quantity' => $newPrice->get('minQuantity'),
                'max_quantity' => $newPrice->get('maxQuantity'),
                'value' => $newPrice->get('value'),
            ];
            if ($index >= $prices->count()) {
                /** @var Price $createdPrice */
                $createdPrice = Price::create($price);
                $prices->push($createdPrice);
            } else {
                $prices[$index]->fill($price);
                $prices[$index]->save();
            }
            $prices[$index]->warehouse()->associate($warehouse);
        }
        if (count($newPrices) < $prices->count()) {
            $index = $prices->count() - 1;
            do {
                /** @var Price $oldPrice */
                $oldPrice = $warehouse->prices->pull($index);
                $oldPrice->delete();
            } while (--$index >= count($newPrices));
        }
        $this->pricesable->setResult($prices);
    }
}
