<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IDownloadFileable;
use App\Traits\CreateCommandTrait;
use Storage;

class DownloadFile implements ICommand
{
    use CreateCommandTrait;

    /**
     * @param IDownloadFileable $fileable
     */
    public function __construct(private readonly IDownloadFileable $fileable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        Storage::put($this->fileable->getFileName(), file_get_contents($this->fileable->getFileUrl()));
    }
}
