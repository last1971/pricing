<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IGoodable;
use App\Models\Good;
use App\Models\Seller;
use App\Traits\CreateCommandTrait;

class MakeGood implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private IGoodable $goodable)
    {
    }

    public function execute(): void
    {
        /** @var Seller $seller */
        $seller = $this->goodable->getSeller();
        /** @var Good $good */
        $good = Good::firstOrCreate(
            [
                'seller_id' => $seller->id,
                'code' => $this->goodable->getCode(),
            ],
            [
                'alias' => $this->goodable->getAlias(),
            ],
        );
        $good->seller()->associate($seller);
        $this->goodable->setGood($good);
    }
}
