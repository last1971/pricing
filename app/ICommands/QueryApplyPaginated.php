<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IQueryAppliable;

class QueryApplyPaginated implements ICommand
{

    public function __construct(private IQueryAppliable $queryAppliable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->queryAppliable->setResult(
            $this->queryAppliable->getQuery()->paginate()
        );
    }
}
