<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IQueryCreateable;
use App\Traits\CreateCommandTrait;
use Illuminate\Database\Eloquent\Model;

class QueryCreate implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private IQueryCreateable $queryable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $modelClass = $this->queryable->getModelClass();
        /** @var Model $model */
        $model = new $modelClass;
        $this->queryable->setQuery($model->newQuery());
    }
}
