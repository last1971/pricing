<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IParseable;
use App\Traits\CreateCommandTrait;

class ApiParser implements \App\Interfaces\ICommand
{
    use CreateCommandTrait;

    public function __construct(private IParseable $parseable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        /** @var ICommand $parser */
        $parser = $this->parseable->getParser()::create($this->parseable->getThis());
        $parser->execute();
    }
}
