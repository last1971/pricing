<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\ISearchPreparable;
use App\Models\Seller;
use App\Traits\CreateCommandTrait;

class SearchPrepare implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private ISearchPreparable $preparable)
    {

    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $sellerIds = $this->preparable->getSellerIds();
        $searchCommands = [];
        if ($this->preparable->getSearchStrategy() === 'search') {
            /** @var Seller $seller */
            foreach ($this->preparable->getUser()->getAllowedApiSellers()->whereIn('id', $sellerIds) as $seller) {
                unset($sellerIds[array_search($seller->id, $sellerIds)]);
                $searchCommands[] = $seller->api_class;
            }
        }
        if (!empty($sellerIds)) {
            $searchCommands[] = SearchInDB::class;
        }
        $this->preparable->setSearchCommands($searchCommands);
        $this->preparable->setSellerIds($sellerIds);
    }
}
