<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IGoodsable;
use App\Traits\CreateCommandTrait;

class MakeGoods implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private IGoodsable $goodsable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        foreach ($this->goodsable->getGoods() as $good) {
            MakeGood::create($good)->execute();
            ApiParameters::create($good)->execute();
            MakeParameters::create($good)->execute();
            MakeWarehouses::create($good)->execute();
        }
    }
}
