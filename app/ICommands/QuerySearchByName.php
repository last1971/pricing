<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IQuerySearchByNameable;
use App\Interfaces\IUObject;
use App\Traits\CreateCommandTrait;
use Illuminate\Database\Eloquent\Builder;

class QuerySearchByName implements ICommand
{

    use CreateCommandTrait;

    public function __construct(private IQuerySearchByNameable $searchByNameQueryable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $search = mb_ereg_replace(
            config('app.search_replace'),
            '',
            $this->searchByNameQueryable->getSearch()
        );
        $query = $this->searchByNameQueryable
            ->getQuery()
            ->whereHas(
                'warehouse.good',
                function (Builder $query) use ($search) {
                    $query->where('alias', 'LIKE', '%' . $search . '%');
                }
            );
        $this->searchByNameQueryable->setQuery($query);
    }
}
