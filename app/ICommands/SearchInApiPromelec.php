<?php

namespace App\ICommands;

use App\ICommands\Parsers\PromelecParser;
use App\Interfaces\ICommand;
use App\Interfaces\IUObject;
use App\Models\Currency;
use App\Models\Seller;
use App\Services\SellerApis\PromelecApiService;
use App\Traits\SearchInApiTrait;

class SearchInApiPromelec implements ICommand
{
    use SearchInApiTrait;

    public function __construct(private IUObject $object)
    {
        $this->object->set('sellerApiService', PromelecApiService::class);
        $this->object->set('sellerApiMethod', 'searchByName');
        $this->object->set('parser', PromelecParser::class);
        $this->object->set('seller', Seller::firstByAlias(Seller::Promelec));
        $this->object->set('currency', Currency::firstByAlfa3('RUB'));
    }
}
