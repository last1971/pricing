<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\ISearchable;

class Search implements ICommand
{

    private ISearchable $searchable;

    public function __construct(ISearchable $searchable)
    {
        $this->searchable = $searchable;
    }

    public function execute(): void
    {
        app()->scoped('SearchResult', fn($app) => $this->searchable->search());
    }
}
