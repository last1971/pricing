<?php

namespace App\ICommands;

use App\ICommands\Parsers\DanParser;
use App\Imports\DanImport;
use App\Interfaces\ICommand;
use App\Interfaces\IUObject;
use App\Models\Currency;
use App\Models\Seller;
use App\Services\UObject;

class DownloadDan implements ICommand
{
    /**
     * @var IUObject
     */
    private IUObject $object;

    public function __construct()
    {
        $this->object = new UObject([
            'fileUrl' => config('seller.dan.fileUrl'),
            'fileName' => 'price.xls',
            'zipPassword' => config('seller.dan.zipPassword'),
            'importClass' => DanImport::class,
            'parser' => DanParser::class,
            'seller' => Seller::firstByAlias(Seller::Dan),
            'currency' => Currency::firstByAlfa3('RUB'),
        ]);
    }

    public function execute(): void
    {
        DownloadFile::create($this->object)->execute();
        UnzipFile::create($this->object)->execute();
        ImportFile::create($this->object)->execute();
        ApiParser::create($this->object)->execute();
        MakeGoods::create($this->object)->execute();
    }
}
