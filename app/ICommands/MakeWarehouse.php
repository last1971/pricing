<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IWarehouseable;
use App\Models\Good;
use App\Models\Warehouse;
use App\Traits\CreateCommandTrait;

class MakeWarehouse implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private IWarehouseable $warehouseable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        /** @var Good $good */
        $good = $this->warehouseable->getGood()->get('good');
        $code = $this->warehouseable->getCode();
        /** @var Warehouse|null $warehouse */
        $warehouse = $good->warehouses->firstWhere('code', $code);
        if ($warehouse) {
            $warehouse->fill(
                [
                    'quantity' => $this->warehouseable->getQuantity(),
                    'delivery_time' => $this->warehouseable->getDeliveryTime(),
                    'multiplicity' => $this->warehouseable->getMultiplicity(),
                ]
            );
            $warehouse->save();
        } else {
            $warehouse = Warehouse::create(
                [
                    'good_id' => $good->id,
                    'code' => $code,
                    'quantity' => $this->warehouseable->getQuantity(),
                    'delivery_time' => $this->warehouseable->getDeliveryTime(),
                    'multiplicity' => $this->warehouseable->getMultiplicity(),
                ],
            );
            //$good->warehouses->push(
            //    $warehouse
            //);
        }
        $warehouse->good()->associate($good);
        $this->warehouseable->setWarehouse($warehouse);
    }
}
