<?php

namespace App\ICommands\Parsers;

use App\Interfaces\IApiParseable;
use App\Interfaces\IUObject;
use App\Models\Currency;
use App\Models\Seller;
use App\Services\UObject;
use App\Traits\CreateCommandTrait;

class PromelecParser implements \App\Interfaces\ICommand
{

    use CreateCommandTrait;

    /**
     * @param IApiParseable $parseable
     */
    public function __construct(private IApiParseable $parseable)
    {
    }

    /**
     * @param array $item
     * @return IUObject[]
     */
    private function calculatePrices(array $item, string $priceUnitKey = ""): array
    {
        $priceUnit = empty($priceUnitKey) ? 1 : $item[$priceUnitKey];
        return array_map(
            fn($index, $price) => new UObject([
                'minQuantity' => max($item['moq'], $price['quant']),
                'maxQuantity' => $index + 1 === count($item['pricebreaks'])
                    ? 0
                    : $item['pricebreaks'][$index + 1]['quant'] - 1,
                'currency' => $this->parseable->getCurrency(),
                'value' => $price['price'] / $priceUnit * config('app.pricePrecision')
            ]),
            array_keys($item['pricebreaks']),
            $item['pricebreaks']
        );
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->parseable->setGoods(
            array_map(
                function (array $item) {
                    $good = new UObject([
                        'keyValues' =>  [
                            'name' => $item['name'],
                            'case' => empty($item['package']) ? null : $item['package'],
                            'producer' => empty($item['producer_name']) ? null : $item['producer_name'],
                            'packageQuantity' => empty($item['pack_quant']) ? 1 : $item['pack_quant'],
                            'shortDescription' => empty($item['description']) ? null : $item['description'],
                        ],
                        'alias' => $item['name'],
                        'code' => $item['item_id'],
                        'seller' => $this->parseable->getSeller(),
                    ]);
                    $warehouses = array_map(
                        fn (array $warehouse) => new UObject([
                            'good' => $good,
                            'code' => $warehouse['vendor'],
                            'quantity' => $warehouse['quant'],
                            'multiplicity' => empty($warehouse['vendor_mpq']) ? 1 : $warehouse['vendor_mpq'],
                            'deliveryTime' => $warehouse['delivery'],
                            'prices' => $this->calculatePrices($warehouse),
                        ]),
                        !empty($item['vendors']) ?$item['vendors'] : [],
                    );
                    $warehouses[] = new UObject([
                        'good' => $good,
                        'code' => null,
                        'quantity' => $item['quant'],
                        'multiplicity' => empty($item['price_unit']) ? 1 : $item['price_unit'],
                        'deliveryTime' => 0,
                        'prices' => $this->calculatePrices($item, 'price_unit'),
                    ]);
                    $good->set('warehouses', $warehouses);
                    return $good;
                },
                $this->parseable->getResponse()
            )
        );
    }
}
