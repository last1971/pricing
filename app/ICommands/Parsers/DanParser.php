<?php

namespace App\ICommands\Parsers;

use App\Interfaces\IApiParseable;
use App\Interfaces\ICommand;
use App\Services\UObject;
use App\Traits\CreateCommandTrait;

class DanParser implements ICommand
{

    use CreateCommandTrait;

    /**
     * @param IApiParseable $parseable
     */
    public function __construct(private IApiParseable $parseable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->parseable->setGoods(
            array_map(
                function (array $item) {
                    $good = new UObject([
                        'keyValues' =>  [
                            'name' => $item['naimenovanie'],
                            'case' => $item['korpus'],
                            'producer' => $item['proizvoditel'],
                            'packageQuantity' => 1,
                            'shortDescription' => $item['xarakteristika'],
                        ],
                        'alias' => $item['naimenovanie'],
                        'code' => $item['kod'],
                        'seller' => $this->parseable->getSeller(),
                    ]);
                    $warehouse = new UObject([
                        'good' => $good,
                        'code' => null,
                        'quantity' => $item['na_sklade'],
                        'multiplicity' => 1,
                        'deliveryTime' => 0,
                    ]);
                    $price = new UObject([
                        'minQuantity' => 1,
                        'maxQuantity' => 0,
                        'currency' => $this->parseable->getCurrency(),
                        'value' => ($item['cena'] ?? 0) * config('app.pricePrecision'),
                    ]);
                    $warehouse->set('prices', [$price]);
                    $good->set('warehouses', [$warehouse]);
                    return $good;
                },
                $this->parseable->getResponse()[0],
            )
        );
    }
}
