<?php

namespace App\ICommands\Parsers;

use App\Interfaces\IApiParseable;
use App\Interfaces\ICommand;
use App\Interfaces\IUObject;
use App\Services\UObject;
use App\Traits\CreateCommandTrait;

class CompelParser implements ICommand
{
    use CreateCommandTrait;

    /**
     * @param IApiParseable $parseable
     */
    public function __construct(private IApiParseable $parseable)
    {
    }

    /**
     * @param array $item
     * @return array
     */
    private function getParameters(array $item): array
    {
        return [
            'name' => $item['item_name'],
            'case' => empty($item['package_name']) ? null : $item['package_name'],
            'producer' => empty($item['item_brend']) ? null : $item['item_brend'],
            'restricted' => $item['pos'],
            'packageQuantity' => empty($item['qty_in_pack']) ? 1 : $item['qty_in_pack'],
        ];
    }

    /**
     * @param array $item
     * @return IUObject
     */
    private function getGood(array $item): IUObject
    {
        return new UObject([
            'seller' => $this->parseable->getSeller(),
            'alias' => $item['item_name'],
            'code' => $item['item_id'],
            'keyValues' => $this->getParameters($item),
        ]);
    }

    /**
     * @param array $location
     * @param IUObject $good
     * @return IUObject
     */
    private function getWarehouse(array $location, IUObject $good): IUObject
    {
        return new UObject([
            'good' => $good,
            'code' => null,
            'quantity' => $location['vend_qty'],
            'multiplicity' => $location['mpq'],
            'deliveryTime' => $location['prognosis_days'] - 1,
        ]);
    }

    /**
     * @param array $price
     * @return IUObject
     */
    private function getPrice(array $price): IUObject
    {
        return new UObject([
            'minQuantity' => $price['min_qty'],
            'maxQuantity' => $price['max_qty'],
            'currency' => $this->parseable->getCurrency(),
            'value' => $price['price'] * config('app.pricePrecision')
        ]);
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->parseable->setGoods(
            array_map(
                function ($item) {
                    $good = $this->getGood($item);
                    $good->set(
                        'warehouses',
                        array_map(
                            function ($location) use ($good) {
                                $warehouse = $this->getWarehouse($location, $good);
                                $warehouse->set(
                                    'prices',
                                    array_map(
                                        fn($price) => $this->getPrice($price),
                                        $location['price_qty'],
                                    )
                                );
                                return $warehouse;
                            },
                            $item['locations'],
                        ),
                    );
                    return $good;
                },
                $this->parseable->getResponse()['result']['items'],
            ),
        );
    }
}
