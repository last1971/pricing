<?php

namespace App\ICommands\Parsers;

use App\Interfaces\IApiParseable;
use App\Interfaces\ICommand;
use App\Services\UObject;
use App\Traits\CreateCommandTrait;

class MarsParser implements ICommand
{

    use CreateCommandTrait;

    /**
     * @param IApiParseable $parseable
     */
    public function __construct(private IApiParseable $parseable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $data = array_filter($this->parseable->getResponse()[0], fn($row) => $row[0]);
        $this->parseable->setGoods(
            array_map(
                function(array $item) {
                    $coeff = $item[2] === 'шт' ? 1 : 1000;
                    $s = str_replace( ',', '.', $item[3]);
                    $pos = strpos($s, '/');
                    if ($pos > 0) {
                        $s = substr($s, 0, $pos);
                    } else if ($pos === 0) {
                        $s = "1";
                    }
                    if ($s === " ") $s=1;
                    $good = new UObject([
                        'keyValues' =>  [
                            'name' => $item[1],
                            'producer' => $item[9],
                            'packageQuantity' => $coeff * $s,
                        ],
                        'alias' => $item[1],
                        'code' => $item[0],
                        'seller' => $this->parseable->getSeller(),
                    ]);
                    $warehouse = new UObject([
                        'good' => $good,
                        'code' => null,
                        'quantity' => $item[4] * $coeff,
                        'multiplicity' => $coeff * $s,
                        'deliveryTime' => 0,
                    ]);
                    $price = new UObject([
                        'minQuantity' => $s * $coeff,
                        'maxQuantity' => 0,
                        'currency' => $this->parseable->getCurrency(),
                        'value' => mb_ereg_replace('[^0-9.]', '', $item[7])
                            / $coeff
                            * config('app.pricePrecision'),
                    ]);
                    $warehouse->set('prices', [$price]);
                    $good->set('warehouses', [$warehouse]);
                    return $good;
                },
                $data
            )
        );
    }
}
