<?php

namespace App\ICommands;

use App\Interfaces\ICommand;

class DoNothingCommand implements ICommand
{
    /**
     * @return void
     */
    public function execute(): void
    {
        // Do nothing
    }
}
