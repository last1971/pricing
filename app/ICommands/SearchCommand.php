<?php

namespace App\ICommands;

use App\Interfaces\IUObject;
use App\Traits\CreateCommandTrait;

class SearchCommand extends MacroCommand
{
    use CreateCommandTrait;

    public function __construct(private IUObject $obj)
    {
        parent::__construct([
            SearchStrategy::create($this->obj),
            SearchPrepare::create($this->obj),
            SearchRun::create($this->obj),
        ]);
    }
}
