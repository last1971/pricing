<?php

namespace App\ICommands;

use App\Interfaces\IUObject;
use App\Services\QuerySearchFabric;

class SearchInDB extends MacroCommand
{
    public function __construct(IUObject $object)
    {
        parent::__construct(commands: [
            QueryCreate::create($object),
            QuerySetWith::create($object),
            QuerySearchAddSellers::create($object),
            QuerySearchFabric::create($object),
            QueryApplyGet::create($object),
        ]);
    }
}
