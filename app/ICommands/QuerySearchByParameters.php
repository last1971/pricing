<?php

namespace App\ICommands;

use App\Interfaces\ICanable;
use App\Interfaces\ICommand;
use App\Interfaces\IQuerySearchByParameterable;
use App\Interfaces\IUObject;
use App\Models\Good;
use App\Models\ParameterValue;
use App\Traits\CreateCommandTrait;
use Illuminate\Database\Eloquent\Builder;

class QuerySearchByParameters implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private IQuerySearchByParameterable $queryable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $mainQuery = $this->queryable
            ->getQuery()
            ->whereIn(
                'good_id',
                Good::select('id')
                    ->whereIn(
                        'product_id',
                        ParameterValue::select('product_id')
                            ->where(function (Builder $query) {
                                foreach ($this->queryable->getParameters() as $parameterNameId => $values) {
                                    foreach ($values as $value) {
                                        $query->orWhere(function (Builder $insideQuery) use ($parameterNameId, $value) {
                                            $insideQuery
                                                ->where('parameter_name_id', $parameterNameId)
                                                ->where('parameter_string_id', $value['parameter_string_id'])
                                                ->where('numeric_value', $value['numeric_value'])
                                                ->where('fraction', $value['fraction'])
                                                ->where('parameter_unit_id', $value['parameter_unit_id']);
                                        });
                                    }
                                }
                            })
                    )
            );
        $this->queryable->setQuery($mainQuery);
    }
}
