<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IParameterable;
use App\Traits\CreateCommandTrait;

class MakeParameters implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private IParameterable $parametrable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $good = $this->parametrable->getGood();
        if ($good->wasRecentlyCreated) {
            $good->parameterValues()->createMany($this->parametrable->getParameters());
        }
        $good->load('parameterValues');
    }
}
