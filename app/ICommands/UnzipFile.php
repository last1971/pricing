<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IUnzipFileable;
use App\Traits\CreateCommandTrait;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class UnzipFile implements ICommand
{
    use CreateCommandTrait;

    /**
     * @param IUnzipFileable $fileable
     */
    public function __construct(private readonly IUnzipFileable $fileable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $zip = new ZipArchive();
        $zip->open(Storage::path($this->fileable->getFileName()));
        $name = $zip->getNameIndex(0);
        $password = $this->fileable->getZipPassword();
        if ($password) {
            $zip->setPassword($password);
        }
        Storage::put($name, $zip->getFromIndex(0));
        $zip->close();
        Storage::delete($this->fileable->getFileName());
        $this->fileable->setFileName($name);
    }
}
