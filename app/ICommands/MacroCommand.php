<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use Illuminate\Contracts\Container\BindingResolutionException;

class MacroCommand implements ICommand
{
    /**
     * @var ICommand[]
     */
    private array $commands;

    /**
     * @param ICommand[] $commands
     */
    public function __construct(array $commands)
    {
        $this->commands = $commands;
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        foreach ($this->commands as $command) {
            $command->execute();
        }
    }
}
