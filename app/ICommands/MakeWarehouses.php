<?php

namespace App\ICommands;


use App\Interfaces\IWarehousesable;
use App\Traits\CreateCommandTrait;

class MakeWarehouses implements \App\Interfaces\ICommand
{
    use CreateCommandTrait;

    public function __construct(private IWarehousesable $warehousesable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        foreach ($this->warehousesable->getWarehouses() as $warehouse) {
            MakeWarehouse::create($warehouse)->execute();
            MakePrices::create($warehouse)->execute();
        }
    }
}
