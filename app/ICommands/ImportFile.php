<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IImportFileable;
use App\Traits\CreateCommandTrait;
use Excel;

class ImportFile implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private IImportFileable $fileable)
    {
    }

    public function execute(): void
    {
        $importClass = $this->fileable->getImportClass();
        $this->fileable->setResponse(
            Excel::toArray(new $importClass, $this->fileable->getFileName())
        );
    }
}
