<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\ISearchStrategyable;
use App\Interfaces\IUObject;
use App\Services\ObjectSearchStrategy;

class SearchStrategy implements ICommand
{
    public function __construct(private ISearchStrategyable $strategyable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->strategyable->setSearchStrategy($this->strategyable->createSearchStrategy());
    }

    public static function create(IUObject $object): ICommand
    {
        return new SearchStrategy(new ObjectSearchStrategy($object));
    }
}
