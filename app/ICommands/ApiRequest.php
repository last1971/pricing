<?php

namespace App\ICommands;

use App\Interfaces\IApiRequestable;
use App\Interfaces\ICommand;
use App\Traits\CreateCommandTrait;

class   ApiRequest implements ICommand
{
    use CreateCommandTrait;

    /**
     * @param IApiRequestable $apiRequestable
     */
    public function __construct(private IApiRequestable $apiRequestable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $apiRequest = app($this->apiRequestable->getSellerApiService());
        $this->apiRequestable->setResponse(
            call_user_func(
                [$apiRequest, $this->apiRequestable->getSellerApiMethod()],
                $this->apiRequestable->getSearch()
            )
        );
    }
}
