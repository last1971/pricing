<?php

namespace App\ICommands;

use App\ICommands\Parsers\MarsParser;
use App\Imports\MarsImport;
use App\Interfaces\ICommand;
use App\Interfaces\IUObject;
use App\Models\Currency;
use App\Models\Seller;
use App\Services\UObject;

class DownloadMars implements ICommand
{
    /**
     * @var IUObject
     */
    private IUObject $object;

    public function __construct()
    {
        $this->object = new UObject([
            'fileUrl' => config('seller.mars.fileUrl'),
            'fileName' => 'mars_full_price_list.xls',
            'importClass' => MarsImport::class,
            'parser' => MarsParser::class,
            'seller' => Seller::firstByAlias(Seller::Mars),
            'currency' => Currency::firstByAlfa3('RUB'),
        ]);
    }

    public function execute(): void
    {
        DownloadFile::create($this->object)->execute();
        ImportFile::create($this->object)->execute();
        ApiParser::create($this->object)->execute();
        MakeGoods::create($this->object)->execute();
    }
}
