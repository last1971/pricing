<?php

namespace App\ICommands;

use App\Interfaces\ITickable;
use App\Services\CommandHandler;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Collection;


class TickWithEnd implements ITickable
{

    /**
     * @var ITickable
     */
    protected ITickable $next;

    /**
     * @var CommandHandler
     */
    protected CommandHandler $handler;

    /**
     * @param ITickable|null $next
     */
    public function __construct(ITickable $next = null)
    {
        $this->handler = new CommandHandler();
        $this->setNextTick($next ?? $this);
    }

    /**
     * @return void
     * @throws BindingResolutionException
     */
    public function execute(): void
    {
        $this->handler->handle();
        $this->nextTick();
    }

    /**
     * @return void
     * @throws BindingResolutionException
     */
    public function nextTick(): void
    {
        /** @var Collection $commandQueue */
        $commandQueue = app()->make('CommandQueue');
        if ($commandQueue->isNotEmpty()) {
            $this->next->execute();
        }
    }

    /**
     * @param ITickable $next
     * @return void
     */
    public function setNextTick(ITickable $next): void
    {
        $this->next = $next;
    }
}
