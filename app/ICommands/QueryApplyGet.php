<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IQueryAppliable;
use App\Traits\CreateCommandTrait;

class QueryApplyGet implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private IQueryAppliable $queryAppliable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->queryAppliable->setResult(
            $this->queryAppliable->getQuery()->get()
        );
    }
}
