<?php

namespace App\ICommands;

use App\Interfaces\ICommand;

use App\Interfaces\IQueryWithable;
use App\Models\Price;
use App\Traits\CreateCommandTrait;

class QuerySetWith implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private IQueryWithable $queryable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->queryable->setQuery(query: $this->queryable->getQuery()->with($this->queryable->getWith()));
    }
}
