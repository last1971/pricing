<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IQuerySellerable;
use App\Traits\CreateCommandTrait;
use Illuminate\Database\Eloquent\Builder;

class QuerySearchAddSellers implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private IQuerySellerable $sellerable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->sellerable->setQuery(
            $this->sellerable
                ->getQuery()
                ->when(!empty($this->sellerable->getSellerIds()), function (Builder $query) {
                    $query->whereHas(
                        'warehouse.good.seller',
                        function (Builder $hasQuery) {
                            $hasQuery->whereIn('id', $this->sellerable->getSellerIds());
                        });
                })
        );
    }
}
