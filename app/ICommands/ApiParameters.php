<?php

namespace App\ICommands;

use App\Interfaces\IApiParameterable;
use App\Interfaces\ICommand;
use App\Models\ParameterName;
use App\Models\ParameterString;
use App\Models\ParameterUnit;
use App\Traits\CreateCommandTrait;
use Closure;

class ApiParameters implements ICommand
{
    use CreateCommandTrait;

    /**
     * @var array
     */
    private array $parameterNameTypes = [
        'name' => ParameterName::String,
        'case' => ParameterName::String,
        'producer' => ParameterName::String,
        'shortDescription' => ParameterName::String,
        'restricted' => ParameterName::Bool,
        'packageQuantity' => ParameterName::Numeric,
    ];

    /**
     * @var Closure[]
     */
    private array $numericValues = [];

    /**
     * @var array
     */
    private array $parameterUnits = [];

    /**
     * @param IApiParameterable $parameterable
     */
    public function __construct(private IApiParameterable $parameterable)
    {
        $this->parameterUnits['packageQuantity'] = ParameterUnit::firstWhere('name', 'pieces');
        $this->numericValues = [
            ParameterName::String => fn() => null,
            ParameterName::Bool => fn($v) => $v ? 1 : 0,
            ParameterName::Numeric => fn($v) => $v,
        ];
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $parameters = [];
        foreach ($this->parameterable->getKeyValues() as $key => $value) {
            if (!is_null($value)) {
                $type = $this->parameterNameTypes[$key];
                $parameterName = ParameterName::firstByNameAndType($key, $type);
                $parameters[] = [
                    'parameter_name_id' => $parameterName->id,
                    'parameter_string_id' => $type == ParameterName::String
                        ? ParameterString::firstOrCreate([
                            'value' => $value, 'parameter_name_id' => $parameterName->id
                        ])->id
                        : null,
                    'numeric_value' => call_user_func($this->numericValues[$type], $value),
                    'parameter_unit_id' => empty($this->parameterUnits[$key]) ? null : $this->parameterUnits[$key]->id,
                ];
            }
        }
        $this->parameterable->setParameters($parameters);
    }
}
