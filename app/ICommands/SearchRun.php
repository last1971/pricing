<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\ISearchRunable;
use App\Interfaces\IUObject;
use App\Traits\CreateCommandTrait;
use Illuminate\Support\Facades\Cache;
use Spatie\Async\Pool;

class SearchRun implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private ISearchRunable $runable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $result = collect();
        /* Asynchronous variant(not testable)
        $pool = Pool::create();
        $hash = $this->runable->getThis()->hash();
        foreach ($this->runable->getSearchCommands() as $commandClass) {
            $searchObj = clone $this->runable->getThis();
            $key = $commandClass . ':' . $hash;
            $pool->add(function () use ($commandClass, $searchObj, $key) {
                $app = require __DIR__ . '/../../bootstrap/app.php';
                $app->make(\Illuminate\Contracts\Console\Kernel::class)->bootstrap();
                if (!Cache::has($key)) {
                    $command = new $commandClass($searchObj);
                    $command->execute();
                    Cache::put($key, $searchObj->get('result'), 60 * 60);
                }
            })
            ->then(function () use (&$result, $key) {
                $cache = Cache::get($key);
                $result = $result->merge($cache);
            });
        }
        $pool->wait();
        $this->runable->setResult($result);
        return;
        //*/
        $hash = $this->runable->getThis()->hash();
        foreach ($this->runable->getSearchCommands() as $commandClass) {
            $key = $commandClass . ':' . $hash;
            $cache = Cache::remember($key, 60 * 60, function () use ($commandClass) {
                $searchObj = clone $this->runable->getThis();
                /** @var ICommand $command */
                $command = new $commandClass($searchObj);
                $command->execute();
                return $searchObj->get('result');
            });
            $result = $result->merge($cache);
        }
        $this->runable->setResult($result);
    }
}
