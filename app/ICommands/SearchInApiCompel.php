<?php

namespace App\ICommands;

use App\ICommands\Parsers\CompelParser;
use App\Interfaces\ICommand;
use App\Interfaces\IUObject;
use App\Models\Currency;
use App\Models\Seller;
use App\Services\SellerApis\CompelApiService;
use App\Traits\SearchInApiTrait;

class SearchInApiCompel implements ICommand
{
    use SearchInApiTrait;

    public function __construct(private IUObject $object)
    {
        $this->object->set('sellerApiService', CompelApiService::class);
        $this->object->set('sellerApiMethod', 'searchInCenter');
        $this->object->set('parser', CompelParser::class);
        $this->object->set('seller', Seller::firstByAlias(Seller::Compel));
        $this->object->set('currency', Currency::firstByAlfa3('USD'));
    }
}
