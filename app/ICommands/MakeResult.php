<?php

namespace App\ICommands;

use App\Interfaces\ICommand;
use App\Interfaces\IResultable;
use App\Interfaces\IUObject;
use App\Models\Good;
use App\Models\Price;
use App\Models\Warehouse;
use App\Services\UObject;
use App\Traits\CreateCommandTrait;

class MakeResult implements ICommand
{
    use CreateCommandTrait;

    public function __construct(private IResultable $resultable)
    {
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->resultable->setResult(
            collect(
                array_map(
                    fn($good) => collect(
                        array_map(
                            fn($warehouse) => $warehouse->get('result'),
                            $good->get('warehouses'),
                        )
                    )->collapse(),
                    $this->resultable->getGoods(),
                )
            )->collapse()
        );
    }
}
