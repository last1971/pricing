<?php

namespace App\Validators;

use Spatie\DataTransferObject\Validation\ValidationResult;

#[\Attribute] class SearchString implements \Spatie\DataTransferObject\Validator
{

    public function __construct(private int $min)
    {
    }

    public function validate(mixed $value): ValidationResult
    {
        if (!$value || strlen($value) < $this->min) {
            return  ValidationResult::invalid("It's too short string for search");
        }
        return ValidationResult::valid();
    }
}
