<?php

namespace App\Console\Commands;

use App\ICommands\DownloadMars;
use Illuminate\Console\Command;

class MarsImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:mars';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Mars Prices';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(DownloadMars $downloadMars)
    {
        $downloadMars->execute();
        return 0;
    }
}
