<?php

namespace App\Console\Commands;

use App\ICommands\DownloadDan;
use Illuminate\Console\Command;

class DanImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:dan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Dan Prices';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $command = new DownloadDan();
        $command->execute();
        return 0;
    }
}
