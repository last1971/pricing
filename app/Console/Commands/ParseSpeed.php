<?php

namespace App\Console\Commands;

use App\ICommands\SearchInApiCompel;
use App\ICommands\SearchInApiPromelec;
use App\Services\UObject;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ParseSpeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:speed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $start = Carbon::now();
        $obj = new UObject([
            'search' => 'RC0805'
        ]);
        $command = new SearchInApiCompel($obj);
        $command->execute();
        $stop = Carbon::now();
        echo $start->diffAsCarbonInterval($stop) .  ' - ' . $obj->get('result')->count() . PHP_EOL;
        return 0;
    }
}
