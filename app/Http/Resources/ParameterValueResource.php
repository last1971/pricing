<?php

namespace App\Http\Resources;

use App\Models\ParameterName;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

/**
 * @property ParameterName $parameterName
 * @property string $parameterValue
 */
class ParameterValueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->parameterName->name,
            'value' => $this->parameterValue,
        ];
    }
}
