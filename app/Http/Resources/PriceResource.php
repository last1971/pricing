<?php

namespace App\Http\Resources;

use App\Models\Warehouse;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

/**
 * @property string $id
 * @property int $value
 * @property int $min_quantity
 * @property int $max_quantity
 * @property int $currency_id
 * @property int $warehouse_id
 * @property Warehouse $warehouse
 */
class PriceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'value' => $this->value / config('app.pricePrecision'),
            'minQuantity' => $this->min_quantity,
            'maxQuantity' => $this->max_quantity,
            'currencyId' => $this->currency_id,
            'sellerId' => $this->warehouse->good->seller_id,
            'quantity' => $this->warehouse->quantity,
            'multiplicity' => $this->warehouse->multiplicity,
            'deliveryTime' => $this->warehouse->delivery_time + $this->warehouse->good->seller->delivery_time,
            'goodId' => $this->warehouse->good_id,
            'alias' => $this->warehouse->good->alias,
            'code' => $this->warehouse->good->code,
            'parameters' => ParameterValueResource::collection($this->warehouse->good->parameterValues),
        ];
    }
}
