<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class AuthOptional extends Middleware
{
    protected function unauthenticated($request, array $guards)
    {
        // do nothing
    }
}
