<?php

namespace App\Http\Requests;

use App\Interfaces\IUObject;
use App\Interfaces\IUObjectable;
use App\Services\UObject;
use Illuminate\Foundation\Http\FormRequest;

class UOjectableRequest extends FormRequest implements IUObjectable
{
    /**
     * @return IUObject
     */
    public function toUObject(): IUObject
    {
        return new UObject(array_merge($this->validated()));
    }
}
