<?php

namespace App\Http\Requests;

use App\Exceptions\ValidationException;
use App\Models\Price;
use App\Models\Seller;
use App\Models\User;
use Illuminate\Contracts\Validation\Validator;

class PriceRequest extends UOjectableRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return void
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'modelClass' => Price::class,
            'user' => $this->user() ?? new User(),
            'with' => ['warehouse.good.parameterValues'],
            'sellerIds' => $this->get('sellerIds') ?? Seller::all()->map(fn($seller) => $seller->id)->toArray(),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'search' => 'required|string|min:3',
            'update' => 'nullable|in:true,false',
            'sellerIds' => 'array',
            'sellerIds.*' => 'int',
            'modelClass' => 'required|string',
            'user' => 'required',
            'with' => 'array'
        ];
    }

    /**
     * @throws ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator->getMessageBag()->all());
    }

}
