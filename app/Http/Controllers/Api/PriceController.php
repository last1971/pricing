<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PriceRequest;
use App\Http\Resources\PriceResource;
use App\ICommands\SearchCommand;

class PriceController extends Controller
{
    /**
     * @param PriceRequest $request
     * @return mixed
     */
    public function index(PriceRequest $request)
    {
        $obj = $request->toUObject();
        $command = SearchCommand::create($obj);
        $command->execute();
        return PriceResource::collection($obj->get('result'));
    }
}
