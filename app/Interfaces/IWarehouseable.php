<?php

namespace App\Interfaces;


use App\Models\Warehouse;

interface IWarehouseable
{
    /**
     * @return string|null
     */
    public function getCode(): mixed;

    /**
     * @return IUObject
     */
    public function getGood(): IUObject;

    /**
     * @return int
     */
    public function getQuantity(): int;

    /**
     * @return int
     */
    public function getDeliveryTime(): int;

    /**
     * @return int
     */
    public function getMultiplicity(): int;

    /**
     * @param Warehouse $warehouse
     * @return void
     */
    public function setWarehouse(Warehouse $warehouse): void;
}
