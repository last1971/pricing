<?php

namespace App\Interfaces;

use Illuminate\Support\Collection;

interface ISearchRunable
{
    /**
     * @return string[]
     */
    public function getSearchCommands(): array;

    /**
     * @return IUObject
     */
    public function getThis(): IUObject;

    /**
     * @param Collection $result
     * @return void
     */
    public function setResult(Collection $result): void;
}
