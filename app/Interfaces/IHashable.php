<?php

namespace App\Interfaces;

interface IHashable
{
    /**
     * @return string
     */
    public function hash(): string;
}
