<?php

namespace App\Interfaces;

interface IDownloadFileable
{
    /**
     * @return string
     */
    public function getFileUrl(): string;

    /**
     * @return string
     */
    public function getFileName(): string;
}
