<?php

namespace App\Interfaces;

interface ITickable extends ICommand
{
    public function nextTick(): void;

    public function setNextTick(ITickable $next): void;
}
