<?php

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Builder;
use JsonSerializable;

interface IQueryAppliable
{
    /**
     * @return Builder
     */
    public function getQuery(): Builder;

    /**
     * @param JsonSerializable $result
     * @return void
     */
    public function setResult(JsonSerializable $result): void;
}
