<?php

namespace App\Interfaces;

interface IGoodsable
{
    /**
     * @return IUObject[]
     */
    public function getGoods(): array;
}
