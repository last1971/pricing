<?php

namespace App\Interfaces;

interface ISearchStrategyable
{
    /**
     * @return string|null
     */
    public function createSearchStrategy(): string|null;

    /**
     * @param string $strategy
     * @return void
     */
    public function setSearchStrategy(string $strategy): void;
}
