<?php

namespace App\Interfaces;

use App\Models\User;

interface ISearchPreparable
{
    /**
     * @return int[]
     */
    public function getSellerIds(): array;

    /**
     * @return User
     */
    public function getUser(): User;

    /**
     * @return string
     */
    public function getSearchStrategy(): string;

    /**
     * @param string[] $searchCommands
     * @return void
     */
    public function setSearchCommands(array $searchCommands): void;

    /**
     * @param array $sellerIds
     * @return void
     */
    public function setSellerIds(array $sellerIds): void;
}
