<?php

namespace App\Interfaces;

use ReflectionMethod;

interface IMethodable
{
    /**
     * @param ReflectionMethod $method
     * @param array $args
     * @return string
     */
    public function createMethod(ReflectionMethod $method, ...$args): string;
}
