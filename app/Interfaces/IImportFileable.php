<?php

namespace App\Interfaces;

interface IImportFileable
{
    /**
     * @return string
     */
    public function getFileName(): string;

    /**
     * @param array $response
     * @return void
     */
    public function setResponse(array $response): void;

    /**
     * @return string
     */
    public function getImportClass(): string;
}
