<?php

namespace App\Interfaces;

use Exception;

interface IExceptionHandler
{
    /**
     * @param ICommand $command
     * @param Exception $exception
     * @return void
     */
    public function handle(ICommand $command, Exception $exception): void;
}
