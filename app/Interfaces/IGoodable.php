<?php

namespace App\Interfaces;

use App\Models\Good;
use App\Models\Seller;

interface IGoodable
{
    public function getSeller(): Seller;

    public function getCode(): string;

    public function getAlias(): string;

    public function setGood(Good $good): void;
}
