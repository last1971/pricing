<?php

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Builder;

interface IQuerySellerable
{
    /**
     * @return int[]|null
     */
    public function getSellerIds(): mixed;

    /**
     * @return Builder
     */
    public function getQuery(): Builder;

    /**
     * @param Builder $query
     * @return void
     */
    public function setQuery(Builder $query): void;
}
