<?php

namespace App\Interfaces;

interface IApiParameterable
{
    public function getKeyValues(): array;

    public function setParameters(array $parameters): void;
}
