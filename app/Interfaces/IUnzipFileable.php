<?php

namespace App\Interfaces;

interface IUnzipFileable
{
    /**
     * @return string
     */
    public function getFileName(): string;

    /**
     * @return string
     */
    public function getZipPassword(): string;

    /**
     * @param string $unzipFileName
     * @return void
     */
    public function setFileName(string $unzipFileName): void;
}
