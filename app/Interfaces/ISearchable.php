<?php

namespace App\Interfaces;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

interface ISearchable
{
    public function search(): AnonymousResourceCollection;
}
