<?php

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Builder;

interface IQueryWithable
{
    /**
     * @return Builder
     */
    public function getQuery(): Builder;

    /**
     * @param Builder $query
     * @return void
     */
    public function setQuery(Builder $query): void;

    /**
     * @return array
     */
    public function getWith(): array;
}
