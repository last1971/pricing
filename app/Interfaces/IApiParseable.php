<?php

namespace App\Interfaces;

use App\Models\Currency;
use App\Models\Seller;

interface IApiParseable
{
    /**
     * @return Seller
     */
    public function getSeller(): Seller;

    /**
     * @return Currency
     */
    public function getCurrency(): Currency;

    /**
     * @return array
     */
    public function getResponse(): array;

    /**
     * @param IUObject[] $parsedObjects
     * @return void
     */
    public function setGoods(array $parsedObjects): void;
}
