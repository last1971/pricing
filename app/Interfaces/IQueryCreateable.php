<?php

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Builder;

interface IQueryCreateable
{
    /**
     * @param Builder $query
     * @return void
     */
    public function setQuery(Builder $query): void;

    /**
     * @return string
     */
    public function getModelClass(): string;
}
