<?php

namespace App\Interfaces;

use ReflectionClass;
use ReflectionMethod;

interface IClassable
{
    public function getClassName(ReflectionClass $class):string;

    public function createConstructor(): string;

    public function createMethod(ReflectionMethod $method): string;
}
