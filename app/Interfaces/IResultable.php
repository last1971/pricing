<?php

namespace App\Interfaces;

use Illuminate\Support\Collection;

interface IResultable
{
    /**
     * @return IUObject[]
     */
    public function getGoods(): array;

    /**
     * @param Collection $result
     * @return void
     */
    public function setResult(Collection $result): void;
}
