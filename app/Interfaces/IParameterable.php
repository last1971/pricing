<?php

namespace App\Interfaces;

use App\Models\Good;

interface IParameterable
{
    /**
     * @return Good
     */
    public function getGood(): Good;

    /**
     * @return array
     */
    public function getParameters(): array;
}
