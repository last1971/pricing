<?php

namespace App\Interfaces;

interface IWarehousesable
{
    /**
     * @return IUObject[]
     */
    public function getWarehouses(): array;
}
