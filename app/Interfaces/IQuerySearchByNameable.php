<?php

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Builder;

interface IQuerySearchByNameable
{
    /**
     * @return string
     */
    public function getSearch(): string;

    /**
     * @return Builder
     */
    public function getQuery(): Builder;

    /**
     * @param Builder $query
     * @return void
     */
    public function setQuery(Builder $query): void;
}
