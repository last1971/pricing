<?php

namespace App\Interfaces;

use App\Models\Price;
use App\Models\Warehouse;
use Illuminate\Support\Collection;

interface IPricesable
{
    /**
     * @return Warehouse
     */
    public function getWarehouse(): Warehouse;

    /**
     * @return IUObject[]
     */
    public function getPrices(): array;

    /**
     * @param Collection<Price> $prices
     * @return void
     */
    public function setResult(Collection $prices): void;
}
