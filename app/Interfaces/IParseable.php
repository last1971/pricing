<?php

namespace App\Interfaces;

interface IParseable
{
    /**
     * @return string
     */
    public function getParser(): string;

    /**
     * @return IUObject
     */
    public function getThis(): IUObject;
}
