<?php

namespace App\Interfaces;

interface IApiRequestable
{
    /**
     * @return string
     */
    public function getSearch(): string;

    /**
     * @return string
     */
    public function getSellerApiService(): string;

    /**
     * @return string
     */
    public function getSellerApiMethod(): string;

    /**
     * @param array $response
     * @return void
     */
    public function setResponse(array $response): void;
}
