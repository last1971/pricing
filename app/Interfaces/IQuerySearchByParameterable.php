<?php

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Builder;

interface IQuerySearchByParameterable
{
    /**
     * @return array
     */
    public function getParameters(): mixed;

    /**
     * @return Builder
     */
    public function getQuery(): Builder;

    /**
     * @param Builder $query
     * @return void
     */
    public function setQuery(Builder $query): void;
}
