<?php

namespace App\Interfaces;

interface IUObjectable
{
    public function toUObject(): IUObject;
}
