<?php

namespace App\Interfaces;

interface ICommand
{
    public function execute(): void;
}
