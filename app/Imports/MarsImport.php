<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithStartRow;


class MarsImport implements WithStartRow
{
    public function startRow(): int
    {
        return 10;
    }
}
