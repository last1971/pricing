<?php

namespace App\Providers;

use App\ICommands\QuerySearchByName;
use App\ICommands\QuerySearchByParameters;
use App\Interfaces\ISearchAttributable;
use App\Services\ExceptionHandlers\MainExceptionHandler;
use App\Services\TwoSearchAttributes;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\ServiceProvider;
use Throwable;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @var Throwable|null
     */
    protected Throwable|null $lastException = null;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        if ($this->app->isLocal()) {
            $this->app->register(IdeHelperServiceProvider::class);
        }
        $this->app->instance('CommandQueue', collect());
        $this->app->instance('ExceptionHandlers', collect());
        $this->app->singleton(MainExceptionHandler::class, fn() => new MainExceptionHandler());
        $this->app->bind('LastException', function($app, $args) {
            $exception = $this->lastException;
            $this->lastException = empty($args) ? null : $args[0];
            return $exception;
        });
        $this->app->instance(
            'PossibleSearchStrategies',
            [
                'search' => QuerySearchByName::class,
                'parameters' => QuerySearchByParameters::class,
            ],
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
