<?php

return array (
    0 =>
        array (
            'item_id' => 41921,
            'name' => 'TDA2003AV',
            'producer_name' => 'STM',
            'country_name' => 'Франция',
            'sort_name' => 'TDA2003',
            'quant' => 150,
            'description' => 'УHЧ 10Вт (14В/2 Ом)',
            'description_eng' => 'PA 10W (14V/2 Ом)',
            'body_weight_netto' => 1.83,
            'package' => 'TO220_5',
            'pack_quant' => 50,
            'packaging' => 'Tube (туба)',
            'mounting_type' => 'Through Hole',
            'class0_id' => 1,
            'class1_id' => 24,
            'class2_id' => 144,
            'class0name' => 'Микросхемы',
            'class1name' => 'Микросхемы для бытовой аппаратуры',
            'class2name' => 'Микросхемы для УНЧ',
            'rkg' => '1 16 2',
            'date_time' => '12.05.2022 22:44:10',
            'price_unit' => 1,
            'datasheet' => 'https://office.promelec.ru/upload/datasheet/5/d/tda2003.pdf',
            'photo_url' => 'https://office.promelec.ru/upload/items/2017/08/29/39707.jpg',
            'analog_key' => 11272,
            'weight' => 3.96,
            'flag_nds' => 0,
            'moq' => 1,
            'pricebreaks' =>
                array (
                    0 =>
                        array (
                            'quant' => 1,
                            'price' => 448.07,
                            'pureprice' => 489.16,
                        ),
                    1 =>
                        array (
                            'quant' => 3,
                            'price' => 417.49,
                            'pureprice' => 448.07,
                        ),
                    2 =>
                        array (
                            'quant' => 5,
                            'price' => 394.0,
                            'pureprice' => 417.49,
                        ),
                    3 =>
                        array (
                            'quant' => 10,
                            'price' => 382.37,
                            'pureprice' => 394.0,
                        ),
                    4 =>
                        array (
                            'quant' => 19,
                            'price' => 368.18,
                            'pureprice' => 382.37,
                        ),
                ),
        ),
    1 =>
        array (
            'item_id' => 158049,
            'name' => 'TDA2003V',
            'producer_name' => 'STM',
            'country_name' => 'Марокко',
            'sort_name' => 'TDA2003',
            'quant' => 542,
            'description' => 'УHЧ 10Вт (14В/2 Ом)',
            'description_eng' => 'PA 10W (14V/2 Ом)',
            'body_weight_netto' => 1.83,
            'package' => 'TO220_5',
            'pack_quant' => 50,
            'packaging' => 'Tube (туба)',
            'class0_id' => 1,
            'class1_id' => 24,
            'class2_id' => 144,
            'class0name' => 'Микросхемы',
            'class1name' => 'Микросхемы для бытовой аппаратуры',
            'class2name' => 'Микросхемы для УНЧ',
            'rkg' => '1 16 2',
            'date_time' => '12.05.2022 22:44:10',
            'price_unit' => 1,
            'datasheet' => 'https://office.promelec.ru/upload/items/2020/01/24/TDA2003.pdf',
            'photo_url' => 'https://office.promelec.ru/upload/items/2017/08/29/39707.jpg',
            'analog_key' => 11272,
            'weight' => 3.65,
            'flag_nds' => 0,
            'moq' => 3,
            'pricebreaks' =>
                array (
                    0 =>
                        array (
                            'quant' => 1,
                            'price' => 110.08,
                            'pureprice' => 122.83,
                        ),
                    1 =>
                        array (
                            'quant' => 9,
                            'price' => 100.59,
                            'pureprice' => 110.08,
                        ),
                    2 =>
                        array (
                            'quant' => 17,
                            'price' => 93.25,
                            'pureprice' => 100.59,
                        ),
                    3 =>
                        array (
                            'quant' => 34,
                            'price' => 89.63,
                            'pureprice' => 93.25,
                        ),
                    4 =>
                        array (
                            'quant' => 100,
                            'price' => 85.28,
                            'pureprice' => 89.63,
                        ),
                ),
            'vendors' =>
                array (
                    0 =>
                        array (
                            'vendor' => 108,
                            'mpq' => 1,
                            'moq' => 43,
                            'quant' => 8198,
                            'delivery' => 10,
                            'flag_eccn' => 0,
                            'pricebreaks' =>
                                array (
                                    0 =>
                                        array (
                                            'quant' => 43,
                                            'price' => 57.92346,
                                            'pureprice' => 59.37078,
                                        ),
                                    1 =>
                                        array (
                                            'quant' => 88,
                                            'price' => 56.476056,
                                            'pureprice' => 57.92346,
                                        ),
                                    2 =>
                                        array (
                                            'quant' => 349,
                                            'price' => 55.511148,
                                            'pureprice' => 56.476056,
                                        ),
                                    3 =>
                                        array (
                                            'quant' => 1742,
                                            'price' => 54.54624,
                                            'pureprice' => 55.028736,
                                        ),
                                ),
                            'comment' => 'Склад поставщика. По статистике срок поставки соблюдается в 84% случаев, для остальных может быть задержка на 1-2 дня.',
                        ),
                    1 =>
                        array (
                            'vendor' => 2,
                            'mpq' => 1,
                            'moq' => 2,
                            'quant' => 163,
                            'delivery' => 2,
                            'pricebreaks' =>
                                array (
                                    0 =>
                                        array (
                                            'quant' => 1,
                                            'price' => 110.08,
                                            'pureprice' => 122.83,
                                        ),
                                    1 =>
                                        array (
                                            'quant' => 9,
                                            'price' => 100.59,
                                            'pureprice' => 110.08,
                                        ),
                                    2 =>
                                        array (
                                            'quant' => 17,
                                            'price' => 93.25,
                                            'pureprice' => 100.59,
                                        ),
                                    3 =>
                                        array (
                                            'quant' => 34,
                                            'price' => 89.63,
                                            'pureprice' => 93.25,
                                        ),
                                    4 =>
                                        array (
                                            'quant' => 100,
                                            'price' => 85.28,
                                            'pureprice' => 89.63,
                                        ),
                                ),
                        ),
                ),
        ),
    2 =>
        array (
            'item_id' => 435699,
            'name' => 'TDA2003L-TB5-T',
            'producer_name' => 'UTC',
            'sort_name' => 'TDA2003L-TB5-T',
            'quant' => 0,
            'pack_quant' => 1,
            'class0_id' => 2543,
            'class1_id' => 2544,
            'class2_id' => 2545,
            'class0name' => 'Заказные позиции',
            'class1name' => 'Заказные позиции',
            'class2name' => 'Заказные позиции',
            'rkg' => '27 1 1',
            'date_time' => '12.05.2022 22:44:10',
            'price_unit' => 1,
            'moq' => 15,
            'pricebreaks' =>
                array (
                    0 =>
                        array (
                            'quant' => 1,
                            'price' => 20.21,
                            'pureprice' => 22.87,
                        ),
                    1 =>
                        array (
                            'quant' => 105,
                            'price' => 17.51,
                            'pureprice' => 20.21,
                        ),
                    2 =>
                        array (
                            'quant' => 209,
                            'price' => 15.23,
                            'pureprice' => 17.51,
                        ),
                    3 =>
                        array (
                            'quant' => 418,
                            'price' => 14.08,
                            'pureprice' => 15.23,
                        ),
                    4 =>
                        array (
                            'quant' => 836,
                            'price' => 13.33,
                            'pureprice' => 14.08,
                        ),
                ),
            'vendors' =>
                array (
                    0 =>
                        array (
                            'vendor' => 16,
                            'mpq' => 1,
                            'moq' => 133,
                            'quant' => 1122,
                            'delivery' => 3,
                            'flag_eccn' => 0,
                            'pricebreaks' =>
                                array (
                                    0 =>
                                        array (
                                            'quant' => 133,
                                            'price' => 32.24466,
                                            'pureprice' => 33.050052,
                                        ),
                                    1 =>
                                        array (
                                            'quant' => 158,
                                            'price' => 31.439268,
                                            'pureprice' => 32.24466,
                                        ),
                                    2 =>
                                        array (
                                            'quant' => 626,
                                            'price' => 30.90234,
                                            'pureprice' => 31.439268,
                                        ),
                                ),
                            'comment' => 'Склад дистрибьютора. По статистике срок поставки соблюдается в 97% случаев, для остальных может быть задержка на 1-2 дня.',
                        ),
                ),
        ),
);
