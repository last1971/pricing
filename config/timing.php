<?php

return [
    'secondsInMinute' => 60,
    'secondsInHalfHour' => 1800,
    'secondsInHour' => 3600,
    'secondsInDay' => 86400,
];
