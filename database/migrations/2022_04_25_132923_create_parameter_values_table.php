<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_values', function (Blueprint $table) {
            $table->id();
            $table->foreignId('parameter_string_id')->nullable();
            $table->bigInteger('numeric_value')->nullable();
            $table->bigInteger('fraction')->default(0);
            $table->foreignId('parameter_name_id');
            $table->foreignId('parameter_unit_id')->nullable();
            $table->foreignId('good_id');
            $table->timestamps();
            $table->unique(
                ['parameter_string_id', 'numeric_value', 'parameter_name_id', 'good_id'],
                'parameter_values_string_id_numeric_value_name_id_good_id_unique'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_values');
    }
};
