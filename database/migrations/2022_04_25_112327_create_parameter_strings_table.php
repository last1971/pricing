<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_strings', function (Blueprint $table) {
            $table->id();
            $table->string('value');
            $table->foreignId('parameter_name_id');
            $table->timestamps();
            $table->unique(['value', 'parameter_name_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_strings');
    }
};
