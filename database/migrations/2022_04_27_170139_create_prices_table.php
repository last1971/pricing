<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedBigInteger('value');
            $table->unsignedBigInteger('min_quantity')->default(1);
            $table->unsignedBigInteger('max_quantity')->default(0);
            $table->foreignId('currency_id');
            $table->foreignId('warehouse_id');
            $table->timestamps();
            $table->unique(['min_quantity', 'max_quantity', 'warehouse_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
};
