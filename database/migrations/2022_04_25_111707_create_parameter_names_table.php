<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameter_names', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('type', ['numeric', 'string', 'complex', 'bool'])->default('numeric');
            $table->foreignId('parameter_unit_id')->nullable();
            $table->timestamps();
            $table->unique(['name', 'type', 'parameter_unit_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_names');
    }
};
