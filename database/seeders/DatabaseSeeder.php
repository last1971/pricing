<?php

namespace Database\Seeders;

use App\Models\Currency;
use App\Models\ParameterUnit;
use App\Models\Seller;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            CurrencySeeder::class,
            RolesAndPermissionsSeeder::class,
            UserSeeder::class,
            SellerSeeder::class,
            ParameterUnitSeeder::class,
            ParameterNameSeeder::class,
        ]);
    }
}
