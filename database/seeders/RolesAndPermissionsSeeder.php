<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::firstOrCreate(['name' => 'seller api compel']);
        Permission::firstOrCreate(['name' => 'seller api promelec']);
        Permission::firstOrCreate(['name' => 'fake permission']);

        $role = Role::firstOrCreate(['name' => 'seller api']);
        $role
            ->givePermissionTo('seller api compel')
            ->givePermissionTo('seller api promelec')
            ->givePermissionTo('fake permission');
    }
}
