<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::firstOrCreate([
            'alfa3' => 'USD',
            'number3' => '840',
            'name' => 'US Dollar'
        ]);

        Currency::firstOrCreate([
            'alfa3' => 'RUB',
            'number3' => '643',
            'name' => 'Russian Ruble'
        ]);
    }
}
