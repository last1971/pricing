<?php

namespace Database\Seeders;

use App\ICommands\SearchInApiCompel;
use App\ICommands\SearchInApiPromelec;
use App\Models\Seller;
use Illuminate\Database\Seeder;

class SellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Seller::firstOrCreate([
            'name' => 'ЗАО "Компэл"',
            'inn' => '7713005406',
            'alias' => 'compel',
            'delivery_time' => 6,
            'api_class' => SearchInApiCompel::class,
        ]);
        Seller::firstOrCreate([
            'name' => 'ООО "ДАН"',
            'inn' => '5503012474',
            'alias' => 'dan',
            'delivery_time' => 3,
        ]);
        Seller::firstOrCreate([
            'name' => 'ООО "ПКФ "МАРС-Компонент"',
            'inn' => '7733139133',
            'alias' => 'mars',
            'delivery_time' => 9,
        ]);
        Seller::firstOrCreate([
            'name' => 'ЗАО "Промэлектроника"',
            'inn' => '6659197470',
            'alias' => 'promelec',
            'delivery_time' => 9,
            'api_class' => SearchInApiPromelec::class,
        ]);
    }
}
