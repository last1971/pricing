<?php

namespace Database\Seeders;

use App\Models\User;
use Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $first = User::firstOrCreate(
            [
                'name' => 'First',
                'email' => 'first@gmail.com',
            ],
            [
                'password' => Hash::make('password'),
            ]
        );
        $second = User::firstOrCreate(
            [
                'name' => 'Second',
                'email' => 'second@gmail.com',
            ],
            [
                'password' => Hash::make('password'),
            ]
        );
        $first->assignRole('seller api');
        $second
            ->givePermissionTo('fake permission')
            ->givePermissionTo('seller api promelec');
    }
}
