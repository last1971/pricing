<?php

namespace Database\Seeders;

use App\Models\Good;
use Illuminate\Database\Seeder;

class GoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Good::all()->each(function (Good $good) {
            $good->alias = mb_ereg_replace(config('app.search_replace'), '', $good->alias);
            $good->save();
        });
    }
}
