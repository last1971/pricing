<?php

namespace Database\Seeders;

use App\Models\ParameterName;
use App\Models\ParameterUnit;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ParameterNameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        ParameterName::firstOrCreate([
            'name' => 'name',
            'type' => ParameterName::String,
        ]);
        ParameterName::firstOrCreate([
            'name' => 'case',
            'type' => ParameterName::String,
        ]);
        ParameterName::firstOrCreate([
            'name' => 'producer',
            'type' => ParameterName::String,
        ]);
        ParameterName::firstOrCreate([
            'name' => 'packageQuantity',
            'type' => ParameterName::Numeric,
            'parameter_unit_id' => ParameterUnit::firstOrCreate([
                'name' => 'pieces',
                'abbreviation' => 'pcs',
                'coefficient' => 1,
                'multiply' => true,
            ])->id,
        ]);
        ParameterName::firstOrCreate([
            'name' => 'restricted',
            'type' => ParameterName::Bool,
        ]);
        ParameterName::firstOrCreate([
            'name' => 'shortDescription',
            'type' => ParameterName::String,
        ]);
    }
}
