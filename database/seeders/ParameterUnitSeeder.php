<?php

namespace Database\Seeders;

use App\Models\ParameterUnit;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ParameterUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ParameterUnit::firstOrCreate([
            'name' => 'pieces',
            'abbreviation' => 'pcs',
            'coefficient' => 1,
            'multiply' => true,
        ]);
    }
}
