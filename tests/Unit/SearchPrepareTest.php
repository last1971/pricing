<?php

namespace Tests\Unit;

use App\ICommands\SearchInApiCompel;
use App\ICommands\SearchInApiPromelec;
use App\ICommands\SearchInDB;
use App\ICommands\SearchPrepare;
use App\Models\Seller;
use App\Models\User;
use App\Services\UObject;
use Database\Seeders\RolesAndPermissionsSeeder;
use Database\Seeders\SellerSeeder;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SearchPrepareTest extends \Tests\TestCase
{
    use DatabaseMigrations;

    /**
     * @var int[]
     */
    private array $sellerIds;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(RolesAndPermissionsSeeder::class);
        $this->seed(UserSeeder::class);
        $this->seed(SellerSeeder::class);
        $this->sellerIds = Seller::all()->map(fn($seller) => $seller->id)->toArray();
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->sellerIds);
    }

    /**
     * @return void
     */
    public function testGuest(): void
    {
        $obj = new UObject([
            'user' => new User(),
            'search' => 'abc',
            'searchStrategy' => 'search',
            'sellerIds' => $this->sellerIds,
        ]);
        $command = SearchPrepare::create($obj);
        $command->execute();
        $this->assertEquals([SearchInDB::class], $obj->get('searchCommands'));
        $this->assertEquals($this->sellerIds, $obj->get('sellerIds'));
    }

    /**
     * @return void
     */
    public function testParameters(): void
    {
        $obj = new UObject([
            'user' => User::firstWhere('name', 'First'),
            'parameters' => [],
            'searchStrategy' => 'parameters',
            'sellerIds' => $this->sellerIds,
        ]);
        $command = SearchPrepare::create($obj);
        $command->execute();
        $this->assertEquals([SearchInDB::class], $obj->get('searchCommands'));
        $this->assertEquals($this->sellerIds, $obj->get('sellerIds'));
    }

    /**
     * @return void
     */
    public function testSearch(): void
    {
        $obj = new UObject([
            'user' => User::firstWhere('name', 'First'),
            'search' => '123',
            'searchStrategy' => 'search',
            'sellerIds' => $this->sellerIds,
        ]);
        $command = SearchPrepare::create($obj);
        $command->execute();
        $this->assertEquals(
            [SearchInApiCompel::class, SearchInApiPromelec::class, SearchInDB::class],
            $obj->get('searchCommands')
        );
        $sellerIds = Seller::whereNotIn('alias', ['compel', 'promelec'])
            ->get()
            ->map(fn($seller) => $seller->id)
            ->toArray();
        $this->assertEquals($sellerIds, array_values($obj->get('sellerIds')));
    }
}
