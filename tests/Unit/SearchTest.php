<?php

namespace Tests\Unit;

use App\Http\Resources\PriceResource;
use App\ICommands\Search;
use App\Interfaces\ISearchable;
use App\Models\Price;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Tests\TestCase;

class SearchTest extends TestCase
{
    /**
     * @var Search
     */
    private Search $search;

    private Price $price;

    /**
     * @var ISearchable
     */
    private ISearchable $searchable;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->price = $this->createMock(Price::class);
        $this->searchable = $this
            ->createMock(ISearchable::class);
        $this->searchable
            ->method('search')
            ->willReturn(PriceResource::collection(collect([$this->price])));
        $this->search = new Search($this->searchable);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->search);
        unset($this->searchable);
        unset($this->priceDTO);
    }

    /**
     * @return void
     * @throws BindingResolutionException
     */
    public function testSearch(): void
    {
        $this->search->execute();
        /** @var AnonymousResourceCollection $result $result */
        $result = app()->make('SearchResult');
        $s = $this->searchable->search();
        $this->assertEquals(new PriceResource($this->price), $result->collection->first());
    }
}
