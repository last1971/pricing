<?php

namespace Tests\Unit;

use App\ICommands\QueryApplyGet;
use App\Interfaces\IQueryAppliable;
use App\Models\Price;
use App\Services\CommandAdapterFabric;
use App\Services\UObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use ReflectionException;

class QueryApplyGetTest extends \Tests\TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     * @throws ReflectionException
     */
    public function testExecute(): void
    {
        $uObject = new UObject([
            'query' => Price::query(),
        ]);
        /** @var IQueryAppliable $adapter */
        $adapter = CommandAdapterFabric::create(IQueryAppliable::class, $uObject);
        $applier = new QueryApplyGet($adapter);
        $applier->execute();
        /** @var Collection $result */
        $result = $uObject->get('result');
        $this->assertTrue($result->isEmpty());
    }
}
