<?php

namespace Tests\Unit;

use App\ICommands\MacroCommand;
use App\Interfaces\ICommand;
use Illuminate\Support\Collection;
use Tests\TestCase;

class MacroCommandTest extends TestCase
{
    public function testExecute(): void
    {
        $command = $this->createMock(ICommand::class);
        $command->expects($this->exactly(3))->method('execute');
        $macroCommand = new MacroCommand([$command, $command, $command]);
        $macroCommand->execute();
    }
}
