<?php

namespace Tests\Unit;

use App\ICommands\QuerySearchAddSellers;
use App\Interfaces\IQuerySellerable;
use App\Models\Price;
use App\Services\CommandAdapterFabric;
use App\Services\UObject;
use ReflectionException;
use Tests\TestCase;

class QuerySearchAddSellersTest extends TestCase
{
    /**
     * @return void
     * @throws ReflectionException
     */
    public function testEmptySellers(): void
    {
        $obj = new UObject(['query' => Price::query()]);
        /** @var IQuerySellerable $sellerable */
        $sellerable = CommandAdapterFabric::create(IQuerySellerable::class, $obj);
        $command = new QuerySearchAddSellers($sellerable);
        $command->execute();
        $this->assertEmpty($obj->get('query')->getBindings());
    }

    /**
     * @return void
     * @throws ReflectionException
     */
    public function testSellers(): void
    {
        $obj = new UObject(['query' => Price::query(), 'sellerIds' => [1, 2, 3]]);
        /** @var IQuerySellerable $sellerable */
        $sellerable = CommandAdapterFabric::create(IQuerySellerable::class, $obj);
        $command = new QuerySearchAddSellers($sellerable);
        $command->execute();
        $this->assertEquals([1, 2, 3], $obj->get('query')->getBindings());
    }
}
