<?php

namespace Tests\Unit;

use App\ICommands\SearchInDB;
use App\ICommands\TickWithEnd;
use App\Models\Currency;
use App\Models\Good;
use App\Models\Price;
use App\Models\Seller;
use App\Models\Warehouse;
use App\Services\UObject;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SearchInDBTest extends TestCase
{
    use RefreshDatabase;

    private string $search;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(SellerSeeder::class);
        $this->seed(CurrencySeeder::class);
        $this->seed(ParameterNameSeeder::class);
        $dan = Seller::firstByAlias('dan');
        $this->search = 'MAX232CPE';
        $good = Good::factory()->create([
            'alias' => $this->search,
            'code' => $this->search,
            'seller_id' => $dan->id,
        ]);
        $usd = Currency::firstByAlfa3('USD');
        $warehouse = Warehouse::firstOrCreate([
            'good_id' => $good->id,
            'quantity' => 100
        ]);
        Price::firstOrCreate([
            'value' => 10000,
            'max_quantity' => 9,
            'currency_id' => $usd->id,
            'warehouse_id' => $warehouse->id,
        ]);
        Price::firstOrCreate([
            'value' => 1000,
            'min_quantity' => 10,
            'currency_id' => $usd->id,
            'warehouse_id' => $warehouse->id,
        ]);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->search);
    }

    /**
     * @throws BindingResolutionException
     */
    public function testExecute(): void
    {
        $this->seed(ParameterNameSeeder::class);
        $obj = new UObject([
            'search' => $this->search,
            'modelClass' => Price::class,
            'with' => ['warehouse.good'],
            'searchStrategy' => 'search',
        ]);
        $search = new SearchInDB($obj);
        $search->execute();
        $this->assertEquals(2, $obj->get('result')->count());
    }
}
