<?php

namespace Tests\Unit;

use App\ICommands\SearchInApiCompel;
use App\ICommands\SearchInDB;
use App\ICommands\SearchRun;
use App\Models\Currency;
use App\Models\Good;
use App\Models\Price;
use App\Models\Seller;
use App\Models\Warehouse;
use App\Services\SellerApis\CompelApiService;
use App\Services\UObject;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class SearchRunTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        Cache::flush();
        $this->seed(SellerSeeder::class);
        $this->seed(CurrencySeeder::class);
        $this->seed(ParameterNameSeeder::class);
        $dan = Seller::firstByAlias('dan');
        $search = 'MAX232CPE';
        $good = Good::factory()->create([
            'alias' => $search,
            'code' => $search,
            'seller_id' => $dan->id,
        ]);
        $usd = Currency::firstByAlfa3('USD');
        $warehouse = Warehouse::firstOrCreate([
            'good_id' => $good->id,
            'quantity' => 100
        ]);
        Price::firstOrCreate([
            'value' => 10000,
            'max_quantity' => 9,
            'currency_id' => $usd->id,
            'warehouse_id' => $warehouse->id,
        ]);
        Price::firstOrCreate([
            'value' => 1000,
            'min_quantity' => 10,
            'currency_id' => $usd->id,
            'warehouse_id' => $warehouse->id,
        ]);
        $compelApi = $this->createMock(CompelApiService::class);
        /** @var array $response */
        $response = require __DIR__ . '/../../storage/compelApiCenterAnswer.php';
        $compelApi
            ->expects($this->atLeast(2))
            ->method('searchInCenter')
            ->willReturnCallback(function(string $search) use ($response) {
                $this->assertEquals('MAX232CPE', $search);
                return $response;
            });
        app()->instance(CompelApiService::class, $compelApi);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $obj = new UObject([
            'search' => 'MAX232CPE',
            'searchStrategy' => 'search',
            'modelClass' => Price::class,
            'with' => ['warehouse.good'],
            'searchCommands' => [
                SearchInApiCompel::class,
                SearchInDB::class,
            ],
            'sellerIds' => Seller::whereNotIn('alias', ['compel'])
                ->get()
                ->map(fn($seller) => $seller->id)
                ->toArray()
        ]);
        $command = SearchRun::create($obj);
        $command->execute();
        /** @var Collection $result */
        $result = $obj->get('result');
        $this->assertEquals(83781, $result->reduce(fn($a, $r) => $r['value'] + $a, 0));
        $command->execute();
        $key = SearchInDB::class . ':' . $obj->hash();
        $this->assertTrue(Cache::has($key));
    }
}
