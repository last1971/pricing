<?php

namespace Tests\Unit;

use App\Services\UObject;
use stdClass;
use Tests\TestCase;

class UObjectTest extends TestCase
{
    public function testHash(): void
    {
        $a = new UObject([
            'int' => 1,
            'float' => 2.1,
            'std' => new stdClass(),
            'string1' => 'abcdefgh',
            'srtring2' => '1234567890',
        ]);
        $b = new UObject([
            'int' => 2,
            'float' => 1.2,
            'std' => [1, 2, 3],
            'string1' => 'abcdefgh',
            'srtring2' => '1234567890',
        ]);
        $this->assertEquals($a->hash(), $b->hash());
    }
}
