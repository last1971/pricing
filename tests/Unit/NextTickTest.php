<?php

namespace Tests\Unit;

use App\ICommands\TickWithEnd;
use App\Interfaces\ICommand;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Collection;
use Tests\TestCase;

class NextTickTest extends TestCase
{
    /**
     * @throws BindingResolutionException
     */
    public function testExecute(): void
    {
        $command = $this->createMock(ICommand::class);
        $command->expects($this->once())->method('execute');
        $exceptionCommand = $this->createMock(ICommand::class);
        $exceptionCommand->expects($this->once())->method('execute');
        $tick = new TickWithEnd();
        /** @var Collection $commandQueue */
        $commandQueue = app()->make('CommandQueue');
        $commandQueue->push($command);
        $commandQueue->push($exceptionCommand);
        $tick->execute();
        $this->assertEquals(0, $commandQueue->count());
    }
}
