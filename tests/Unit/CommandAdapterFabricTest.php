<?php

namespace Tests\Unit;

use App\ICommands\QuerySearchByName;
use App\Interfaces\IQuerySearchByNameable;
use App\Models\Price;
use App\Services\CommandAdapterFabric;
use App\Services\UObject;
use Database\Seeders\ParameterNameSeeder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ReflectionException;
use Tests\TestCase;

class CommandAdapterFabricTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(ParameterNameSeeder::class);
    }

    /**
     * @return void
     * @throws ReflectionException
     */
    public function testGenerate(): void
    {
        $uObject = new UObject([
            'search' => '123',
            'query' => Price::query(),
        ]);
        $adapter = CommandAdapterFabric::create(IQuerySearchByNameable::class, $uObject);
        $search = new QuerySearchByName($adapter);
        $search->execute();
        /** @var Builder $query */
        $query = $uObject->get('query');
        $this->assertEquals(['%123%'], $query->getBindings());
    }
}

