<?php

namespace Tests\Unit;

use App\ICommands\MakeWarehouses;
use App\Models\Currency;
use App\Models\Good;
use App\Models\Seller;
use App\Services\UObject;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MakeWarehousesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(SellerSeeder::class);
        $this->seed(CurrencySeeder::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $seller = Seller::firstByAlias(Seller::Compel);
        $good = Good::create([
            'seller_id' => $seller->id,
            'alias' => 'abc',
            'code' => '123',
        ]);
        $usd = Currency::firstByAlfa3('USD');
        $prices1 = [
            new UObject([
                'currency' => $usd,
                'value' => 10000,
                'minQuantity' => 1,
                'maxQuantity' => 9,
            ]),
            new UObject([
                'currency' => $usd,
                'value' => 9000,
                'minQuantity' => 10,
                'maxQuantity' => 0,
            ]),
        ];
        $prices2 = [
            new UObject([
                'currency' => $usd,
                'value' => 8000,
                'minQuantity' => 10,
                'maxQuantity' => 90,
            ]),
            new UObject([
                'currency' => $usd,
                'value' => 6000,
                'minQuantity' => 100,
                'maxQuantity' => 0,
            ]),
        ];
        $warehouse1 = new UObject([
            'good' => new UObject(compact('good')),
            'code' => null,
            'quantity' => 102,
            'multiplicity' => 2,
            'deliveryTime' => 3,
            'prices' => $prices1,
        ]);
        $warehouse2 = new UObject([
            'good' => new UObject(compact('good')),
            'code' => 'far away',
            'quantity' => 1000,
            'multiplicity' => 10,
            'deliveryTime' => 30,
            'prices' => $prices2,
        ]);
        $obj = new UObject([
            'warehouses' => [$warehouse1, $warehouse2],
        ]);
        $command  = MakeWarehouses::create($obj);
        $command->execute();
        $this->assertNotEmpty($warehouse1->get('warehouse')->prices);
        $this->assertDatabaseHas('warehouses', [
            'code' => 'far away',
            'quantity' => 1000,
            'multiplicity' => 10,
            'delivery_time' => 30,
        ]);
        $this->assertDatabaseHas('prices', [
            'value' => 9000,
            'min_quantity' => 10,
            'max_quantity' => 0,
            'warehouse_id' => $warehouse1->get('warehouse')->id,
        ]);
        $this->assertDatabaseHas('prices', [
            'value' => 8000,
            'min_quantity' => 10,
            'max_quantity' => 90,
            'warehouse_id' => $warehouse2->get('warehouse')->id,
        ]);
    }
}
