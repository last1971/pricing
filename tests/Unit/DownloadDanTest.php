<?php

namespace Tests\Unit;

use App\ICommands\DownloadDan;
use App\Models\Good;
use App\Models\Price;
use App\Models\Warehouse;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DownloadDanTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        config([
            'seller.dan.fileUrl' => __DIR__ . '/../../storage/dan_dealer.xls.zip',
            'seller.dan.zipPassword' => '',
        ]);
        $this->seed(SellerSeeder::class);
        $this->seed(ParameterNameSeeder::class);
        $this->seed(CurrencySeeder::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $command = new DownloadDan();
        $command->execute();
        $this->assertDatabaseHas('goods', [
            'alias' => 'АнтеннаGPSGPSAMPSGSM3mRG174SMAP',
            'code' => '202392',
        ]);
        $this->assertDatabaseHas('warehouses', [
            'good_id' => 1,
            'quantity' => 9,
        ]);
        $this->assertDatabaseHas('prices', [
            'warehouse_id' => 1,
            'value' => 7540000,
        ]);
    }
}
