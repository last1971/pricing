<?php

namespace Tests\Unit;

use App\ICommands\Parsers\CompelParser;
use App\Models\Currency;
use App\Models\Seller;
use App\Services\UObject;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CompelParserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(SellerSeeder::class);
        $this->seed(ParameterNameSeeder::class);
        $this->seed(CurrencySeeder::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $response = require __DIR__ . '/../../storage/compelApiCenterAnswer.php';
        $seller = Seller::firstByAlias(Seller::Compel);
        $currency = Currency::firstByAlfa3('USD');
        $obj = new UObject(compact('response', 'seller', 'currency'));
        $command = CompelParser::create($obj);
        $command->execute();
        $goods = $obj->get('goods');
        $this->assertIsArray($goods);
        $this->assertNotEmpty($goods);
        $this->assertTrue($goods[0] instanceof UObject);
    }
}
