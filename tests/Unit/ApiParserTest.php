<?php

namespace Tests\Unit;

use App\ICommands\ApiParser;
use App\ICommands\Parsers\CompelParser;
use App\Interfaces\ICommand;
use App\Interfaces\IUObject;
use App\Models\Currency;
use App\Models\Seller;
use App\Services\UObject;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ApiParserTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @var IUObject
     */
    private IUObject $object;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(SellerSeeder::class);
        $this->seed(CurrencySeeder::class);
        $response = require __DIR__ . '/../../storage/compelApiCenterAnswer.php';
        $parser = CompelParser::class;
        $seller = Seller::firstByAlias(Seller::Compel);
        $currency = Currency::firstByAlfa3('USD');
        $this->object = new UObject(compact('response', 'parser', 'seller', 'currency'));
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->object);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $command = ApiParser::create($this->object);
        $command->execute();
        $googs = $this->object->get('goods');
        $this->assertNotEmpty($googs);
        $this->assertTrue($googs[0] instanceof IUObject);
    }
}
