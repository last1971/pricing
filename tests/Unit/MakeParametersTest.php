<?php

namespace Tests\Unit;

use App\ICommands\MakeParameters;
use App\Models\Good;
use App\Models\ParameterName;
use App\Models\ParameterString;
use App\Models\Seller;
use App\Services\UObject;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\ParameterUnitSeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MakeParametersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(SellerSeeder::class);
        $this->seed(ParameterUnitSeeder::class);
        $this->seed(ParameterNameSeeder::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $seller = Seller::firstByAlias(Seller::Compel);
        $good = Good::create([
            'seller_id' => $seller->id,
            'alias' => 'abc',
            'code' => '123',
        ]);
        $name = ParameterName::firstByNameAndType('name', ParameterName::String);
        $restricted = ParameterName::firstByNameAndType('restricted', ParameterName::Bool);
        $packageQuantity = ParameterName::firstByNameAndType('packageQuantity', ParameterName::Numeric);
        $parameters = [
            [
                'parameter_name_id' => $name->id,
                'parameter_string_id' => ParameterString::firstOrCreate([
                    'parameter_name_id' => $name->id,
                    'value' => 'abc',
                ])->id,
            ],
            [
                'parameter_name_id' => $restricted->id,
                'numeric_value' => 0,
            ],
            [
                'parameter_name_id' => $packageQuantity->id,
                'numeric_value' => 100,
                'parameter_unit_id' => $packageQuantity->parameter_unit_id,
            ],
        ];
        $obj = new UObject(compact('good', 'parameters'));
        $command = MakeParameters::create($obj);
        $command->execute();
        $this->assertNotEmpty($good->parameterValues);
        $this->assertDatabaseHas('parameter_strings', [
            'value' => 'abc',
        ]);
        $this->assertDatabaseHas('parameter_values', [
            'numeric_value' => 100,
            'parameter_unit_id' => $packageQuantity->parameter_unit_id,
        ]);
    }
}
