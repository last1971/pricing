<?php

namespace Tests\Unit;

use App\ICommands\MakeWarehouse;
use App\Models\Good;
use App\Models\Seller;
use App\Models\Warehouse;
use App\Services\UObject;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MakeWarehouseTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(SellerSeeder::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $seller = Seller::firstByAlias(Seller::Compel);
        $good = Good::create([
            'seller_id' => $seller->id,
            'alias' => 'abc',
            'code' => '123',
        ]);
        $obj = new UObject([
            'good' => new UObject(compact('good')),
            'code' => null,
            'quantity' => 102,
            'multiplicity' => 2,
            'deliveryTime' => 3,
        ]);
        $command = MakeWarehouse::create($obj);
        $command->execute();
        $this->assertTrue($obj->get('warehouse') instanceof Warehouse);
        $this->assertDatabaseHas('warehouses', [
            'code' => null,
            'quantity' => 102,
            'multiplicity' => 2,
            'delivery_time' => 3,
        ]);
    }
}
