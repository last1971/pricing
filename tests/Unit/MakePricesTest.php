<?php

namespace Tests\Unit;

use App\ICommands\MakePrices;
use App\Models\Currency;
use App\Models\Good;
use App\Models\Seller;
use App\Models\Warehouse;
use App\Services\UObject;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MakePricesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(SellerSeeder::class);
        $this->seed(CurrencySeeder::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $seller = Seller::firstByAlias(Seller::Dan);
        $good = Good::create([
            'seller_id' => $seller->id,
            'code' => '12345',
            'alias' => 'TDA2003',
        ]);
        $warehouse = Warehouse::create([
            'good_id' => $good->id,
            'quantity' => 123,
        ]);
        $usd = Currency::firstByAlfa3('USD');
        $prices = [
            new UObject([
                'currency' => $usd,
                'value' => 10000,
                'minQuantity' => 1,
                'maxQuantity' => 9,
            ]),
            new UObject([
                'currency' => $usd,
                'value' => 9000,
                'minQuantity' => 10,
                'maxQuantity' => 0,
            ]),
        ];
        $obj = new UObject(compact('warehouse', 'prices'));
        $command = MakePrices::create($obj);
        $command->execute();
        $this->assertEquals(2, $warehouse->prices->count());
        $this->assertDatabaseHas('prices', [
            'value' => 9000,
            'min_quantity' => 10,
            'max_quantity' => 0,
        ]);
        $prices = [
            new UObject([
                'currency' => $usd,
                'value' => 10000,
                'minQuantity' => 1,
                'maxQuantity' => 9,
            ]),
            new UObject([
                'currency' => $usd,
                'value' => 9000,
                'minQuantity' => 10,
                'maxQuantity' => 99,
            ]),
            new UObject([
                'currency' => $usd,
                'value' => 8000,
                'minQuantity' => 100,
                'maxQuantity' => 0,
            ]),
        ];
        $obj->set('prices', $prices);
        $command->execute();
        $this->assertEquals(3, $warehouse->prices->count());
        $this->assertDatabaseHas('prices', [
            'value' => 9000,
            'min_quantity' => 10,
            'max_quantity' => 99,
        ]);
        $prices = [
            new UObject([
                'currency' => $usd,
                'value' => 10000,
                'minQuantity' => 1,
                'maxQuantity' => 0,
            ]),
        ];
        $obj->set('prices', $prices);
        $command->execute();
        $this->assertEquals(1, $warehouse->prices->count());
        $this->assertDatabaseHas('prices', [
            'value' => 10000,
            'min_quantity' => 1,
            'max_quantity' => 0,
        ]);
        $this->assertDatabaseMissing('prices', [
            'value' => 9000,
            'min_quantity' => 10,
            'max_quantity' => 99,
        ]);
        $command->execute();
    }
}
