<?php

namespace Tests\Unit;

use App\Interfaces\ICommand;
use App\Interfaces\IExceptionHandler;
use App\Services\CommandHandler;
use App\Services\ExceptionHandlers\MainExceptionHandler;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Collection;
use Tests\TestCase;

class CommandHandlerTest extends TestCase
{
    /**
     * @var Exception
     */
    private Exception $exception;

    /**
     * @var ICommand
     */
    private ICommand $commandException;

    /**
     * @throws BindingResolutionException
     */
    public function setUp(): void
    {
        parent::setUp();
        /** @var Collection $commandQueue */
        $commandQueue = app()->make('CommandQueue');
        $this->exception = new Exception('Test');
        $this->commandException = $this->createMock(ICommand::class);
        $this->commandException->method('execute')->willThrowException($this->exception);
        $exceptionHandler = $this->createMock(IExceptionHandler::class);
        $exceptionHandler
            ->expects($this->once())
            ->method('handle')
            ->willReturnCallback(function(ICommand $command, Exception $exception) {
                $this->assertEquals($this->commandException, $command);
                $this->assertEquals($this->exception, $exception);
            });
        MainExceptionHandler::addExceptionHandler(
            get_class($this->commandException), get_class($this->exception), $exceptionHandler
        );
        $command = $this->createMock(ICommand::class);
        $command->expects($this->once())->method('execute');
        $commandQueue->push($this->commandException, $command);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->commandException);
        unset($this->exception);
    }

    /**
     * @throws BindingResolutionException
     */
    public function testHandle(): void
    {
        $handler = new CommandHandler();
        $handler->handle();
        /** @var Collection $commandQueue */
        $commandQueue = app()->make('CommandQueue');
        $this->assertEquals(1, $commandQueue->count());
        $handler->handle();
    }
}
