<?php

namespace Tests\Unit;

use App\ICommands\QuerySetWith;
use App\Interfaces\IQueryWithable;
use App\Models\Price;
use Illuminate\Database\Eloquent\Builder;
use Tests\TestCase;

class QuerySetWithTest extends TestCase
{
    /**
     * @return void
     */
    public function testExecute(): void
    {
        $withable = $this->createMock(IQueryWithable::class);
        $withable->expects($this->once())->method('getWith')->willReturn(['good']);
        $withable->expects($this->once())->method('getQuery')->willReturn(Price::query());
        $withable
            ->expects($this->once())
            ->method('setQuery')
            ->willReturnCallback(function (Builder $query) {
                $this->assertEquals(['good'], array_keys($query->getEagerLoads()));
            });
        $command = new QuerySetWith($withable);
        $command->execute();
    }
}
