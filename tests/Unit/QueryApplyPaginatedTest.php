<?php

namespace Tests\Unit;

use App\ICommands\QueryApplyPaginated;
use App\ICommands\QueryApplyGet;
use App\Interfaces\IQueryAppliable;
use App\Models\Price;
use App\Services\CommandAdapterFabric;
use App\Services\UObject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Testing\Fluent\AssertableJson;
use ReflectionException;
use Tests\TestCase;

class QueryApplyPaginatedTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     * @throws ReflectionException
     */
    public function testExecute(): void
    {
        $uObject = new UObject([
            'query' => Price::query(),
        ]);
        /** @var IQueryAppliable $adapter */
        $adapter = CommandAdapterFabric::create(IQueryAppliable::class, $uObject);
        $applier = new QueryApplyPaginated($adapter);
        $applier->execute();
        /** @var LengthAwarePaginator $result */
        $result = $uObject->get('result');
        $this->assertTrue($result->isEmpty());
        $this->assertEquals(0, $result->get('total'));
    }
}
