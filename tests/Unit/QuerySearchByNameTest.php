<?php

namespace Tests\Unit;

use App\ICommands\QuerySearchByName;
use App\Interfaces\IQuerySearchByNameable;
use App\Models\Price;
use Database\Seeders\ParameterNameSeeder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class QuerySearchByNameTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var QuerySearchByName
     */
    private QuerySearchByName $searchByNameQueryBuilder;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(ParameterNameSeeder::class);

        $searchByNameQueryable = $this->createMock(IQuerySearchByNameable::class);
        $searchByNameQueryable
            ->method('setQuery')
            ->willReturnCallback(function(Builder $query) {
                $this->assertEquals(["%%"], $query->getBindings());
            });
        $searchByNameQueryable
            ->method('getQuery')
            ->willReturn(Price::query());
        $this->searchByNameQueryBuilder = new QuerySearchByName($searchByNameQueryable);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->searchByNameQueryBuilder);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $this->searchByNameQueryBuilder->execute();
    }
}
