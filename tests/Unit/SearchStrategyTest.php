<?php

namespace Tests\Unit;

use App\ICommands\SearchStrategy;
use App\Services\UObject;
use Tests\TestCase;

class SearchStrategyTest extends TestCase
{
    /**
     * @return void
     */
    public function testExecute(): void
    {
        $strategy = 'parameters';
        $obj = new UObject([$strategy => 'test']);
        $command = SearchStrategy::create($obj);
        $command->execute();
        $this->assertEquals($strategy, $obj->get('searchStrategy'));
    }
}
