<?php

namespace Tests\Unit;

use App\ICommands\ImportFile;
use App\Imports\DanImport;
use App\Services\UObject;
use Tests\TestCase;

class ImportFileTest extends TestCase
{
    /**
     * @return void
     */
    public function testCreate(): void
    {
        $obj = new UObject([
            'fileName' => __DIR__ . '/../../storage/dan_dealer.xls',
            'importClass' => DanImport::class,
        ]);
        $command = ImportFile::create($obj);
        $command->execute();
        $this->assertIsArray($obj->get('response'));
        $this->assertEquals(10, count($obj->get('response')[0]));
    }
}
