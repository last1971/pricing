<?php

namespace Tests\Unit;

use App\ICommands\MakeGood;
use App\Models\Seller;
use App\Services\UObject;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MakeGoodTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(SellerSeeder::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $seller = Seller::firstByAlias(Seller::Compel);
        $code = '123';
        $alias = '123';
        $obj = new UObject(compact('seller', 'code', 'alias'));
        $command = MakeGood::create($obj);
        $command->execute();
        $this->assertDatabaseHas('goods', [
            'alias' => '123',
            'code' => '123',
        ]);
    }
}
