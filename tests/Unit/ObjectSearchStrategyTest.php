<?php

namespace Tests\Unit;

use App\Services\ObjectSearchStrategy;
use App\Services\UObject;
use Tests\TestCase;
use TypeError;

class ObjectSearchStrategyTest extends TestCase
{
    /**
     * @return void
     */
    public function testException(): void
    {
        $obj = new UObject();
        $searchStrategy = new ObjectSearchStrategy($obj);
        $this->expectException(TypeError::class);
        $searchStrategy->setSearchStrategy($searchStrategy->createSearchStrategy());
    }

    /**
     * @return void
     */
    public function testCreate(): void
    {
        $strategy = 'search';
        $obj = new UObject([$strategy => 'test']);
        $searchStrategy = new ObjectSearchStrategy($obj);
        $searchStrategy->setSearchStrategy($searchStrategy->createSearchStrategy());
        $this->assertEquals($strategy, $obj->get('searchStrategy'));
    }
}
