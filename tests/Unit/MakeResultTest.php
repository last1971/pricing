<?php

namespace Tests\Unit;

use App\ICommands\ApiParser;
use App\ICommands\MakeGoods;
use App\ICommands\MakePrices;
use App\ICommands\MakeResult;
use App\ICommands\Parsers\CompelParser;
use App\Interfaces\IUObject;
use App\Models\Currency;
use App\Models\Good;
use App\Models\Price;
use App\Models\Seller;
use App\Models\Warehouse;
use App\Services\UObject;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\ParameterUnitSeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Tests\TestCase;

class MakeResultTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var IUObject|UObject
     */
    private IUObject $object;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(SellerSeeder::class);
        $this->seed(CurrencySeeder::class);
        $this->seed(ParameterUnitSeeder::class);
        $this->seed(ParameterNameSeeder::class);
        $response = require __DIR__ . '/../../storage/compelApiCenterAnswer.php';
        $parser = CompelParser::class;
        $seller = Seller::firstByAlias(Seller::Compel);
        $currency = Currency::firstByAlfa3('USD');
        $this->object = new UObject(compact('response', 'parser', 'seller', 'currency'));
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        ApiParser::create($this->object)->execute();
        $command = MakeGoods::create($this->object);
        $command->execute();
        $command = MakeResult::create($this->object);
        $command->execute();
        /** @var Collection $result */
        $result = $this->object->get('result');
        $this->assertNotEmpty($result);
        $this->assertEquals(3, $result->count());
        $this->assertTrue($result->first() instanceof Price);
    }
}
