<?php

namespace Tests\Unit;

use App\ICommands\ApiRequest;
use App\Services\SellerApis\CompelApiService;
use App\Services\UObject;
use Tests\TestCase;

class ApiRequestTest extends TestCase
{
    /**
     * @return void
     */
    public function testExecute(): void
    {
        $compelApi = $this->createMock(CompelApiService::class);
        /** @var array $response */
        $response = require __DIR__ . '/../../storage/compelApiCenterAnswer.php';
        $compelApi
            ->expects($this->once())
            ->method('searchInCenter')
            ->willReturnCallback(function(string $search) use ($response) {
                $this->assertEquals('max232cpe', $search);
                return $response;
            });
        app()->instance(CompelApiService::class, $compelApi);
        $obj = new UObject([
            'search' => 'max232cpe',
            'sellerApiService' => CompelApiService::class,
            'sellerApiMethod' => 'searchInCenter'
        ]);
        $command = ApiRequest::create($obj);
        $command->execute();
        $this->assertEquals($response, $obj->get('response'));
    }
}
