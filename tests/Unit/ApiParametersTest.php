<?php

namespace Tests\Unit;

use App\ICommands\ApiParameters;
use App\Services\UObject;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\ParameterUnitSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ApiParametersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(ParameterUnitSeeder::class);
        $this->seed(ParameterNameSeeder::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $response = require __DIR__ . '/../../storage/compelApiCenterAnswer.php';
        $item = $response['result']['items'][0];
        $obj = new UObject([
            'keyValues' => [
                'name' => $item['item_name'],
                'case' => empty($item['package_name']) ? null : $item['package_name'],
                'producer' => empty($item['item_brend']) ? null : $item['item_brend'],
                'restricted' => $item['pos'],
                'packageQuantity' => empty($item['qty_in_pack']) ? 1 : $item['qty_in_pack'],
            ],
        ]);
        $command = ApiParameters::create($obj);
        $command->execute();
        $parameters = $obj->get('parameters');
        $this->assertEquals(
            [
                'parameter_name_id' => 1,
                'parameter_string_id' => 1,
                'numeric_value' => null,
                'parameter_unit_id' => null,
            ],
            $parameters[0]
        );
        $this->assertEquals(
            [
                'parameter_name_id' => 4,
                'parameter_string_id' => null,
                'numeric_value' => 25,
                'parameter_unit_id' => 1,
            ],
            $parameters[4]
        );
    }
}
