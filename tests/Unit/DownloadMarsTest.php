<?php

namespace Tests\Unit;

use App\ICommands\DownloadMars;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DownloadMarsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        config([
            'seller.mars.fileUrl' => __DIR__ . '/../../storage/mars_full_price_list.xls',
        ]);
        $this->seed(SellerSeeder::class);
        $this->seed(ParameterNameSeeder::class);
        $this->seed(CurrencySeeder::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $command = new DownloadMars();
        $command->execute();
        $this->assertDatabaseHas('goods', [
            'alias' => 'PLSM32планарнаяпанельдлямикросхемы',
            'code' => '00009006',
        ]);
        $this->assertDatabaseHas('warehouses', [
            'good_id' => 1,
            'quantity' => 5173,
        ]);
        $this->assertDatabaseHas('prices', [
            'warehouse_id' => 1,
            'value' => 36523,
        ]);
    }
}
