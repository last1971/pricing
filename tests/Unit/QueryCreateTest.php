<?php

namespace Tests\Unit;

use App\ICommands\QueryCreate;
use App\Interfaces\IQueryCreateable;
use App\Models\Price;
use App\Services\CommandAdapterFabric;
use App\Services\UObject;
use Tests\TestCase;

class QueryCreateTest extends TestCase
{
    /**
     * @return void
     * @throws \ReflectionException
     */
    public function testExecute(): void
    {
        $object = new UObject(['modelClass' => Price::class]);
        $queryable = CommandAdapterFabric::create(IQueryCreateable::class, $object);
        $command = new QueryCreate($queryable);
        $command->execute();
        $this->assertEquals(Price::query(), $object->get('query'));
    }

    public function testApp(): void
    {
        $object = new UObject(['modelClass' => Price::class]);
        $command = QueryCreate::create($object);
        $command->execute();
        $this->assertEquals(Price::query(), $object->get('query'));
    }
}
