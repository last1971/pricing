<?php

namespace Tests\Unit;

use Illuminate\Support\Collection;

class CommandQueueTest extends \Tests\TestCase
{
    public function testQueue(): void
    {
        /** @var Collection $q1 */
        $q1 = app()->make('CommandQueue');
        $q1->push(1);
        $q2 = app()->make('CommandQueue');
        $this->assertEquals($q1, $q2);
    }
}
