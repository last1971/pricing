<?php

namespace Tests\Unit;

use App\ICommands\Parsers\PromelecParser;
use App\Models\Currency;
use App\Models\Seller;
use App\Services\UObject;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PromelecParserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(SellerSeeder::class);
        $this->seed(ParameterNameSeeder::class);
        $this->seed(CurrencySeeder::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $response = require __DIR__ . '/../../storage/promelecApiAnswer.php';
        $seller = Seller::firstByAlias(Seller::Promelec);
        $currency = Currency::firstByAlfa3('RUB');
        $obj = new UObject(compact('response', 'seller', 'currency'));
        $command = PromelecParser::create($obj);
        $command->execute();
        $parsedObjects = $obj->get('goods');
        $this->assertIsArray($parsedObjects);
        $this->assertNotEmpty($parsedObjects);
        $this->assertTrue($parsedObjects[0] instanceof UObject);
    }
}
