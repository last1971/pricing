<?php

namespace Tests\Unit;

use App\ICommands\QuerySearchByParameters;
use App\Interfaces\IQuerySearchByParameterable;
use App\Models\Price;
use App\Services\CommandAdapterFabric;
use App\Services\UObject;
use ReflectionException;
use Tests\TestCase;

class QuerySearchByParametersTest extends TestCase
{
    /**
     * @return void
     * @throws ReflectionException
     */
    public function testExecute(): void
    {
        $obj = new UObject([
            'query' => Price::query(),
            'parameters' => [
                125 => [
                    [
                        'parameter_string_id' => 1,
                        'numeric_value' => null,
                        'fraction' => null,
                        'parameter_unit_id' => null
                    ],
                    [
                        'parameter_string_id' => 10,
                        'numeric_value' => null,
                        'fraction' => null,
                        'parameter_unit_id' => null
                    ],
                ],
                256 => [
                    [
                        'parameter_string_id' => null,
                        'numeric_value' => 12345,
                        'fraction' => 2,
                        'parameter_unit_id' => 1
                    ],
                ],
            ],
        ]);
        /** @var IQuerySearchByParameterable $queryable */
        $queryable = CommandAdapterFabric::create(IQuerySearchByParameterable::class, $obj);
        $command = new QuerySearchByParameters($queryable);
        $command->execute();
        $this->assertEquals([125, 1, 125, 10, 256, 12345, 2, 1], $obj->get('query')->getBindings());
    }
}
