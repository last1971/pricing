<?php

namespace Tests\Unit;

use App\ICommands\QuerySearchByParameters;
use App\Models\Price;
use App\Services\QuerySearchFabric;
use App\Services\UObject;
use Tests\TestCase;

class QuerySearchFabricTest extends TestCase
{
    /**
     * @return void
     */
    public function testCreate(): void
    {
        $obj = new UObject([
            'query' => Price::query(),
            'searchStrategy' => 'parameters',
            'parameters' => [
                125 => [
                    [
                        'parameter_string_id' => 1,
                        'numeric_value' => null,
                        'fraction' => null,
                        'parameter_unit_id' => null
                    ],
                    [
                        'parameter_string_id' => 10,
                        'numeric_value' => null,
                        'fraction' => null,
                        'parameter_unit_id' => null
                    ],
                ],
                256 => [
                    [
                        'parameter_string_id' => null,
                        'numeric_value' => 12345,
                        'fraction' => 2,
                        'parameter_unit_id' => 1
                    ],
                ],
            ],
        ]);
        $command = QuerySearchFabric::create($obj);
        $this->assertTrue($command instanceof QuerySearchByParameters);
    }
}
