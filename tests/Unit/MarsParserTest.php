<?php

namespace Tests\Unit;

use App\ICommands\ImportFile;
use App\ICommands\Parsers\MarsParser;
use App\Imports\MarsImport;
use App\Models\Currency;
use App\Models\Seller;
use App\Services\UObject;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MarsParserTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(SellerSeeder::class);
        $this->seed(ParameterNameSeeder::class);
        $this->seed(CurrencySeeder::class);
    }

    /**
     * @return void
     */
    public function testCreate(): void
    {
        $seller = Seller::firstByAlias(Seller::Mars);
        $currency = Currency::firstByAlfa3('RUB');
        $fileName = __DIR__ . '/../../storage/mars_full_price_list.xls';
        $importClass = MarsImport::class;
        $obj = new UObject(compact('fileName', 'importClass', 'seller', 'currency'));
        $command = ImportFile::create($obj);
        $command->execute();
        $command = MarsParser::create($obj);
        $command->execute();
        $parsedObjects = $obj->get('goods');
        $this->assertIsArray($parsedObjects);
        $this->assertNotEmpty($parsedObjects);
        $this->assertTrue(current($parsedObjects) instanceof UObject);
    }
}
