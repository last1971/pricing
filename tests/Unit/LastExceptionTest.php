<?php

namespace Tests\Unit;

use Exception;

class LastExceptionTest extends \Tests\TestCase
{
    /**
     * @return void
     */
    public function testApp(): void
    {
        $this->assertNull(app('LastException'));
        $exception = new Exception('Test');
        app('LastException', [$exception]);
        $this->assertEquals($exception, app('LastException'));
        $this->assertNull(app('LastException'));
    }
}
