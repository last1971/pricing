<?php

namespace Tests\Unit;

use App\ICommands\SearchCommand;
use App\Models\Currency;
use App\Models\Good;
use App\Models\Price;
use App\Models\Seller;
use App\Models\User;
use App\Models\Warehouse;
use App\Services\SellerApis\CompelApiService;
use App\Services\SellerApis\PromelecApiService;
use App\Services\UObject;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\RolesAndPermissionsSeeder;
use Database\Seeders\SellerSeeder;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class SearchCommandTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(RolesAndPermissionsSeeder::class);
        $this->seed(UserSeeder::class);
        $this->seed(SellerSeeder::class);
        $this->seed(CurrencySeeder::class);
        $this->seed(ParameterNameSeeder::class);
        $dan = Seller::firstByAlias('dan');
        $search = 'MAX232CPE';
        $good = Good::factory()->create([
            'alias' => $search,
            'code' => $search,
            'seller_id' => $dan->id,
        ]);
        $usd = Currency::firstByAlfa3('USD');
        $warehouse = Warehouse::firstOrCreate([
            'good_id' => $good->id,
            'quantity' => 100
        ]);
        Price::firstOrCreate([
            'value' => 10000,
            'max_quantity' => 9,
            'currency_id' => $usd->id,
            'warehouse_id' => $warehouse->id,
        ]);
        Price::firstOrCreate([
            'value' => 1000,
            'min_quantity' => 10,
            'currency_id' => $usd->id,
            'warehouse_id' => $warehouse->id,
        ]);
        $compelApi = $this->createMock(CompelApiService::class);
        /** @var array $response */
        $response = require __DIR__ . '/../../storage/compelApiCenterAnswer.php';
        $compelApi
            ->expects($this->atMost(1))
            ->method('searchInCenter')
            ->willReturn($response);
        app()->instance(CompelApiService::class, $compelApi);
        $promelecApi = $this->createMock(PromelecApiService::class);
        /** @var array $response */
        $promelecResponse = require __DIR__ . '/../../storage/promelecApiAnswer.php';
        $promelecApi
            ->expects($this->atMost(1))
            ->method('searchByName')
            ->willReturn($promelecResponse);
        app()->instance(PromelecApiService::class, $promelecApi);
    }

    /**
     * @return void
     */
    public function testGuestExecute(): void
    {
        $obj = new UObject([
            'search' => '123',
            'modelClass' => Price::class,
            'with' => ['warehouse.good'],
            'user' => new User(),
            'sellerIds' => Seller::all()->map(fn($seller) => $seller->id)->toArray(),
        ]);
        $command = SearchCommand::create($obj);
        $command->execute();
        $this->assertEmpty($obj->get('result')->count());
    }

    /**
     * @return void
     */
    public function testFirstExecute(): void
    {
        $obj = new UObject([
            'search' => 'MAX232CPE',
            'modelClass' => Price::class,
            'with' => ['warehouse.good'],
            'user' => User::firstWhere('name', 'First'),
            'sellerIds' => Seller::all()->map(fn($seller) => $seller->id)->toArray(),
        ]);
        $command = SearchCommand::create($obj);
        $command->execute();
        $this->assertEquals(32, $obj->get('result')->count());
    }

    /**
     * @return void
     */
    public function testSecondExecute(): void
    {
        $obj = new UObject([
            'search' => 'MAX232CPE',
            'modelClass' => Price::class,
            'with' => ['warehouse.good'],
            'user' => User::firstWhere('name', 'Second'),
            'sellerIds' => Seller::all()->map(fn($seller) => $seller->id)->toArray(),
        ]);
        $command = SearchCommand::create($obj);
        $command->execute();
        $this->assertEquals(29, $obj->get('result')->count());
    }
}
