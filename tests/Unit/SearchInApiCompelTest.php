<?php

namespace Tests\Unit;

use App\ICommands\SearchInApiCompel;
use App\Services\SellerApis\CompelApiService;
use App\Services\UObject;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\SellerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SearchInApiCompelTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(SellerSeeder::class);
        $this->seed(ParameterNameSeeder::class);
        $this->seed(CurrencySeeder::class);
    }

    /**
     * @return void
     */
    public function testExecute(): void
    {
        $obj = new UObject(['search' => 'max232cpe']);
        $compelApi = $this->createMock(CompelApiService::class);
        /** @var array $response */
        $response = require __DIR__ . '/../../storage/compelApiCenterAnswer.php';
        $compelApi
            ->expects($this->once())
            ->method('searchInCenter')
            ->willReturnCallback(function(string $search) use ($response) {
                $this->assertEquals('max232cpe', $search);
                return $response;
            });
        app()->instance(CompelApiService::class, $compelApi);
        $command = new SearchInApiCompel($obj);
        $command->execute();
        $result = $obj->get('result');
        $this->assertEquals(3, $result->count());
        $this->assertDatabaseHas('prices', [
            'warehouse_id' => 2,
            'value' => 21111.0,
            'min_quantity' => 3,
            'max_quantity' => 0,
        ]);
        $this->assertDatabaseHas('prices', [
            'warehouse_id' => 2,
            'value' => 26387.0,
            'min_quantity' => 1,
            'max_quantity' => 2,
        ]);
        $this->assertDatabaseHas('warehouses', [
            'quantity' => 319,
            'code' => null,
        ]);
        $this->assertDatabaseHas('goods', [
            'alias' => 'MAX232CPE',
            'code' => '218059',
        ]);
        $this->assertDatabaseHas('parameter_strings', [
            'value' => 'MAX',
        ]);
        $this->assertDatabaseHas('parameter_values', [
            'numeric_value' => 25,
            'parameter_unit_id' => 1,
        ]);
    }
}
