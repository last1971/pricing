<?php

namespace Tests\Feature;

use App\Models\ParameterName;
use Database\Seeders\ParameterNameSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParameterNameTest extends \Tests\TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(ParameterNameSeeder::class);
    }

    public function testSeeder(): void
    {
        $name = ParameterName::firstByNameAndType('name');
        $this->assertEquals(1, $name->id);
    }

}
