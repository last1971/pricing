<?php

namespace Tests\Feature;

use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_use_successful_response()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
