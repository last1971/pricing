<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Laravel\Passport\Client;
use Laravel\Passport\Database\Factories\ClientFactory;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    private Client $client;

    private User $user;

    public function setUp(): void
    {
        parent::setUp();
        Artisan::call('passport:install', ['-vvv' => true]);
        $this->user = User::factory()->create(["password" => \Hash::make("test_password")]);
        /** @var client */
        $this->client = ClientFactory::new()->asPasswordClient()->create(['user_id' => $this->user->id]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->client);
        unset($this->user);
    }

    public function testUserSuccessfulLogin(): void
    {
        $response = $this->post(
            '/oauth/token',
            [
                "grant_type" => "password",
                "client_id" => $this->client->id,
                "client_secret" => $this->client->secret,
                "username" => $this->user->email,
                "password" => "test_password",
            ]
        );
        $response->assertStatus(200)
            ->assertJsonStructure(['token_type', 'refresh_token', 'access_token', 'expires_in']);
        $response = $this->post(
            '/oauth/token',
            [
                "grant_type" => "refresh_token",
                "client_id" => $this->client->id,
                "client_secret" => $this->client->secret,
                "refresh_token" => $response->json('refresh_token'),
            ]
        );
        $response->assertStatus(200)
            ->assertJsonStructure(['token_type', 'refresh_token', 'access_token', 'expires_in']);
    }

    /**
     * @return void
     */
    public function testGetAllowedApiSellers(): void
    {
        $user = new User();
        $this->assertEmpty($user->getAllowedApiSellers());
    }

}
