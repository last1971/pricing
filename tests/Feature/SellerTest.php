<?php

namespace Tests\Feature;

use App\Models\Seller;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class SellerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Seller::factory()->count(3)->create();
    }

    /**
     * @throws \Throwable
     */
    public function testIndex(): void
    {
        $response = $this->getJson('/api/seller');
        $response->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) =>
                $json->has('current_page')
                    ->has('data', 3)
                    ->has('first_page_url')
                    ->has('from')
                    ->has('last_page')
                    ->has('last_page_url')
                    ->has('path')
                    ->has('per_page')
                    ->has('links')
                    ->has('next_page_url')
                    ->has('prev_page_url')
                    ->has('to')
                    ->has('total')
            );
    }
}
