<?php

namespace Tests\Feature;

use App\Models\Currency;
use App\Models\Good;
use App\Models\Price;
use App\Models\Seller;
use App\Models\User;
use App\Models\Warehouse;
use Database\Seeders\CurrencySeeder;
use Database\Seeders\ParameterNameSeeder;
use Database\Seeders\RolesAndPermissionsSeeder;
use Database\Seeders\SellerSeeder;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class PriceTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(RolesAndPermissionsSeeder::class);
        $this->seed(UserSeeder::class);
        $this->seed(SellerSeeder::class);
        $this->seed(CurrencySeeder::class);
        $this->seed(ParameterNameSeeder::class);
        $dan = Seller::firstByAlias('dan');
        $search = 'MAX232CPE';
        $good = Good::factory()->create([
            'alias' => $search,
            'code' => $search,
            'seller_id' => $dan->id,
        ]);
        $usd = Currency::firstByAlfa3('USD');
        $warehouse = Warehouse::firstOrCreate([
            'good_id' => $good->id,
            'quantity' => 100
        ]);
        Price::firstOrCreate([
            'value' => 10000,
            'max_quantity' => 9,
            'currency_id' => $usd->id,
            'warehouse_id' => $warehouse->id,
        ]);
        Price::firstOrCreate([
            'value' => 1000,
            'min_quantity' => 10,
            'currency_id' => $usd->id,
            'warehouse_id' => $warehouse->id,
        ]);
    }

    /**
     * @return void
     */
    public function testFromDB(): void
    {
        $prices = Price::all();
        $this->assertEquals(2, $prices->count());
    }

    /**
     * @return void
     */
    public function testIndex422(): void
    {
        $headers = ['Accept' => 'application/json'];
        $response = $this->get('/api/price', $headers);
        $response->assertStatus(422);
    }

    /**
     * @return void
     */
    public function testIndex200(): void
    {
        $response = $this->getJson(route('price.index', ['search' => '1234']));
        $response->assertStatus(200);
    }
}
