<?php

namespace Tests\Feature;

use App\ICommands\DownloadFile;
use App\ICommands\UnzipFile;
use App\Interfaces\IDownloadFileable;
use App\Interfaces\IUnzipFileable;
use App\Services\CommandAdapterFabric;
use App\Services\UObject;
use ReflectionException;
use Storage;
use Tests\TestCase;

class DownloadAndUnzipFileTest extends TestCase
{
    /**
     * @return void
     * @throws ReflectionException
     */
    public function testExecute(): void
    {
        $fileObj = new UObject([
            'fileUrl' => __DIR__ . '/../../storage/dan_dealer.xls.zip',
            'fileName' => 'test.zip',
            'zipPassword' => '',
        ]);
        /** @var IDownloadFileable $fileable */
        $fileable = CommandAdapterFabric::create(IDownloadFileable::class, $fileObj);
        $command = new DownloadFile($fileable);
        $command->execute();
        $this->assertTrue(Storage::exists($fileable->getFileName()));
        /** @var IUnzipFileable $fileable */
        $fileable = CommandAdapterFabric::create(IUnzipFileable::class, $fileObj);
        $command = new UnzipFile($fileable);
        $command->execute();
        $this->assertTrue(Storage::exists($fileable->getFileName()));
        Storage::delete($fileable->getFileName());
    }
}
