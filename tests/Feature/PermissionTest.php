<?php

namespace Tests\Feature;

use App\Models\Seller;
use App\Models\User;
use Database\Seeders\RolesAndPermissionsSeeder;
use Database\Seeders\SellerSeeder;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Str;
use Tests\TestCase;

class PermissionTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(RolesAndPermissionsSeeder::class);
        $this->seed(UserSeeder::class);
        $this->seed(SellerSeeder::class);
    }

    /**
     * @return void
     */
    public function testPermissions(): void
    {
        $first = User::firstWhere('name', 'First');
        $firstSellers = $first->getAllowedApiSellers();
        $compel = Seller::firstByAlias('compel');
        $promelec = Seller::firstByAlias('promelec');
        $this->assertTrue($firstSellers->contains($compel));
        $second = User::firstWhere('name', 'Second');
        $secondSellers = $second->getAllowedApiSellers();
        $this->assertTrue($firstSellers->contains($promelec));
    }
}
