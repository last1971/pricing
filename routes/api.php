<?php

use App\Http\Controllers\Api\PriceController;
use App\Http\Controllers\Api\SellerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('auth.optional:api')->group(function(){
    Route::controller(PriceController::class)->group(function () {
        Route::get('/price', 'index')->name('price.index');
    });
    Route::controller(SellerController::class)->group(function () {
        Route::get('/seller', 'index')->name('seller.index');
    });
});

Route::fallback(function(){
    return response()->json(['message' => 'Page Not Found'], 404);
});
