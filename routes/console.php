<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('sandbox', function () {
    //dd(__DIR__ .'/../storage/dan_dealer.xls');
    $a=1;
    //dd((new \App\Imports\MarsImport())->toCollection(__DIR__ .'/../storage/mars_full_price_list.xls'));
    dd(Excel::toArray(new \App\Imports\MarsImport(), __DIR__ .'/../storage/mars_full_price_list.xls'));
});
